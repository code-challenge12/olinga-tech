const CircularDependencyPlugin = require('circular-dependency-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')

// @ts-check

/**
 * @type {import('next').NextConfig}
 **/
const nextConfig = {
  webpack: (config, { dev, isServer }) => {
    if (dev && !isServer) {
      config.plugins.push(
        new ESLintPlugin({
          extensions: ['js', 'jsx', 'ts', 'tsx'],
          emitWarning: dev,
          failOnError: false,
        }),
      )

      config.plugins.push(
        new CircularDependencyPlugin({
          exclude: /a\.js|node_modules/,
          failOnError: true,
          allowAsyncCycles: false,
          cwd: process.cwd(),
        }),
      )
    }

    // make svg importable
    const fileLoaderRule = config.module.rules.find(
      (rule) =>
        rule &&
        rule.test &&
        !Array.isArray(rule.test) &&
        rule.test.test('.svg'),
    )

    if (fileLoaderRule) {
      fileLoaderRule.exclude = /\.svg$/
    }

    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack', 'url-loader'],
    })

    return config
  },
  extends: './tsconfig.json',
}

module.exports = nextConfig
