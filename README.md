# Olinga Tech challenge

## Getting Started

### To run the development server:

```bash
yarn dev
```

Open [http://localhost:8081](http://localhost:8081) on your browser to see the
result.

---

### To run the production server:

first run the build command

```bash
yarn build
```

then run start command

```bash
yarn start
```

Open [http://localhost:8081](http://localhost:8081) on your browser to see the
result.

---

### To run the storybook:

```bash
yarn storybook:start
```

Open [http://localhost:6006](http://localhost:6006) on your browser to see the
result.

---

### To run unit testing:

```bash
yarn test
```

---

Feel free to look into `package.json` for any additional script commands
