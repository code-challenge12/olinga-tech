import { useRouter } from 'next/router'

import * as ROUTES from '@/routes'
import type { Link as LinkType } from '@/routes'

export function useActiveRoute() {
  const { pathname: routePathname, asPath: routeAsPath } = useRouter()

  return (pathname: string, exact = false) => {
    const route = (ROUTES as Dict<LinkType>)[pathname] ?? { pathname }

    if (exact) {
      return routeAsPath === route.pathname
    }

    return routePathname === route.pathname
  }
}
