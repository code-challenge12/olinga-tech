import { Link as ReactScrollLink } from 'react-scroll'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import { SystemStyleObject, Link as CKLink } from '@chakra-ui/react'

import * as ROUTES from '@/routes'
import type { Link as LinkType } from '@/routes'

export type RouteKey = keyof typeof ROUTES

export type ScrollOption = {
  spy?: boolean
  smooth?: boolean
  hashSpy?: boolean
  duration?: number
  offset?: number
  activeClass?: string
}

type CommonProps = {
  sx?: SystemStyleObject
  children?: React.ReactNode
  onClick?: React.MouseEventHandler<any>
  scrollOption?: ScrollOption
  passHref?: boolean
  legacyBehavior?: boolean
}

export type URL = RequireAtLeastOne<{
  href: LinkType | string
  to: RouteKey
}>

export const SCROLL_OPTION: ScrollOption = {
  spy: false,
  smooth: true,
  hashSpy: true,
  duration: 500,
  offset: -60,
}

export function Link({
  to,
  href,
  scrollOption,
  passHref = true,
  sx,
  onClick,
  ...rest
}: CommonProps & URL): React.ReactElement {
  const { pathname } = useRouter()

  const _url =
    typeof href === 'string'
      ? {
          pathname: href,
          isExternal: true,
        }
      : href
      ? href
      : to
      ? (ROUTES as Dict<LinkType>)[to]
      : null

  if (!_url?.pathname) {
    throw new Error('Link href/to must not be empty.')
  }

  if (
    (_url.pathname === pathname || _url.pathname === 'currentPath') &&
    _url.hash
  ) {
    return (
      <ReactScrollLink
        {...SCROLL_OPTION}
        {...scrollOption}
        {...rest}
        onClick={onClick as () => void}
        to={_url.hash}
        href={_url.hash === 'top' ? '#' : `#${_url.hash}`}
      />
    )
  }

  if (_url.isExternal) {
    return (
      <CKLink
        isExternal
        sx={sx}
        onClick={onClick}
        {...rest}
        href={_url.pathname}
      />
    )
  }

  return (
    <NextLink href={_url} passHref={passHref} onClick={onClick} {...rest} />
  )
}
