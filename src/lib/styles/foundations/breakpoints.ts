const breakpointsObject = {
  xs: '375px',
  sm: '768px',
  md: '1280px',
  lg: '1680px',
  xl: '1920px',
  '2xl': '2560px',
}

export const breakpoints = breakpointsObject
