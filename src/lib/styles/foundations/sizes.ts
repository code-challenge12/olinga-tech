export const sizes = {
  navBarHeight: {
    base: '64px',
    md: '72px',
  },
}

export const spaces = sizes
