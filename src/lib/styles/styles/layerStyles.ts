export const layerStyles = {
  actionable: {
    opacity: 1,
    cursor: 'pointer',
    transition: 'opacity 150ms',
    '&:hover': {
      opacity: 0.8,
    },
    '&:focus': {
      opacity: 0.65,
    },
    '&:active': {
      opacity: 0.5,
    },
  },
  disabled: {
    opacity: 0.5,
    cursor: 'not-allowed',
  },
  container: {
    minW: '320px',
    maxW: {
      base: 'full',
      sm: 'container.sm',
      md: 'container.md',
    },
    width: 'full',
    padding: {
      base: 8,
      sm: 10,
    },
  },
  pageWrapper: {
    pb: { base: 16, sm: 32 },
    pt: { base: 10, sm: 20 },
    mt: {
      base: 'var(--chakra-sizes-navBarHeight-base)',
      md: 'var(--chakra-sizes-navBarHeight-md)',
    },
  },
}
