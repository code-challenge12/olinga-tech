import { extendTheme } from '@chakra-ui/react'

import * as foundations from './foundations'
import * as components from './components'
import * as styles from './styles'

export const defaultTheme = extendTheme({
  styles: {
    global: () => ({
      'html, body': {
        fontWeight: 'normal',
        color: 'gray.900',
        bgColor: 'white',
      },

      a: {
        textDecoration: 'none',
        color: 'inherit',
      },

      '*': {
        boxSizing: 'border-box',
        outline: 'none',
        lineHeight: 1.5,
        WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
      },

      '#__next': {
        minHeight: '100vh',
        maxWidth: '100%',
        overflow: 'auto',
      },

      'svg *': {
        transformBox: 'fill-box',
      },
    }),
  },
  ...styles,
  ...foundations,
  components: { ...components },
})
