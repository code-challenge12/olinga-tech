const baseStyle = {
  bg: 'gray.800',
  color: 'gray.50',
}

export default {
  baseStyle,
}
