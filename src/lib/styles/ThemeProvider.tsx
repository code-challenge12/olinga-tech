import { ChakraProvider } from '@chakra-ui/react'

import { defaultTheme } from './themes'

interface ThemeProviderProps {
  children: React.ReactNode
}

export function ThemeProvider(props: ThemeProviderProps): React.ReactElement {
  return (
    <ChakraProvider resetCSS theme={defaultTheme}>
      {props.children}
    </ChakraProvider>
  )
}
