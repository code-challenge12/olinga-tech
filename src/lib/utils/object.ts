import { camelToTitleCase } from './format'

export function keyToTitleCase(source: Dict) {
  return Object.fromEntries(
    Object.entries(source).map(([key, value]) => [
      camelToTitleCase(key),
      value,
    ]),
  )
}

export function isEmpty(source: any) {
  if (typeof source === 'object' && Array.isArray(source)) {
    return !source.length
  }

  if (typeof source === 'object') {
    return !Object.keys(source).length
  }

  return false
}
