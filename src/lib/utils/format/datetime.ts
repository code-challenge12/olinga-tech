import dayjs, { Dayjs } from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.extend(relativeTime)
dayjs.tz.setDefault('Asia/Bangkok')

type Day = Dayjs | Date | string | number

function checkNotDayObject(day: Day) {
  return typeof day !== 'object' || !(day instanceof Dayjs)
}

export function getDateString(day: Day): string {
  if (checkNotDayObject(day)) {
    day = dayjs(day)
  }

  return (day as Dayjs).tz().format('MMMM DD, YYYY')
}

export function getDatetimeString(day: Day): string {
  if (checkNotDayObject(day)) {
    day = dayjs(day)
  }

  return (day as Dayjs).tz().format('YYYY-MM-DD HH:mm:ss')
}
