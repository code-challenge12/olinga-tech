export function clamp(value: number, min: number, max: number) {
  return Math.min(Math.max(value, min), max)
}

export function normalize(value: number, min: number, max: number) {
  return (value - min) / (max - min)
}

export function polarToCartesian(
  centerX: number,
  centerY: number,
  radius: number,
  angleInDegrees: number,
) {
  const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0

  return [
    centerX + radius * Math.cos(angleInRadians),
    centerY + radius * Math.sin(angleInRadians),
  ]
}

export function random(min: number, max: number) {
  if (max <= min)
    throw new Error(`max (${max}) should be greater than min (${min})`)

  const gap = max - min

  return Math.round(Math.random() * gap) + min
}
