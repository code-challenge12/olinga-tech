export function getItem<T = any>(key: string): T | null {
  if (!window) return null

  const value = window.localStorage.getItem(key)

  if (!value) return null

  try {
    return JSON.parse(value)
  } catch {
    return value as T
  }
}

export function setItem(key: string, value: any) {
  if (!window) return null

  window.localStorage.setItem(
    key,
    typeof value === 'string' ? value : JSON.stringify(value),
  )
}
