import axios from './axios'

function init(apiUrl: string): void {
  axios(apiUrl)
}

export default init
