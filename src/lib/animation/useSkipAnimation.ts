import { useEffect } from 'react'
import { useReducedMotion, Globals } from '@react-spring/web'

export function useSkipAnimation() {
  const isPreferReducedMotion = useReducedMotion()

  useEffect(() => {
    Globals.assign({
      skipAnimation: Boolean(isPreferReducedMotion),
    })
  }, [isPreferReducedMotion])
}
