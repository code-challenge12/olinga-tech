import { config, animated } from '@react-spring/web'
import { Box, Text, Icon } from '@chakra-ui/react'

export const DEFAULT_SPRING_CONFIG = config.stiff
export const AnimatedBox = animated(Box)
export const AnimatedText = animated(Text)
export const AnimatedIcon = animated(Icon)

export * from './useSkipAnimation'
