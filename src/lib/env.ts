import init from '@/lib/_init'

const branch = process.env.AWS_BRANCH || process.env.CIRCLE_BRANCH

const envObj = {
  API_URL: process.env.API_URL || process.env.NEXT_PUBLIC_API_URL || '',
  APP_URL:
    process.env.APP_URL ||
    process.env.NEXT_PUBLIC_APP_URL ||
    'http://localhost:8081',

  IS_DEV: process.env.NODE_ENV === 'development',
  IS_PROD: false,
}

if (branch === 'production') {
  envObj.IS_PROD = true
}

init(envObj.API_URL)

export default { ...envObj }
