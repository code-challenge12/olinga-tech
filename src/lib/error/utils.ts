import { AxiosError } from 'axios'

const DEFAULT_ERROR_MESSAGE = 'Something went wrong'

export function getErrorMsg(error: any) {
  if (error.response) {
    return error.response.data?.status ?? DEFAULT_ERROR_MESSAGE
  } else if (error.request) {
    return 'Network Error'
  }

  return DEFAULT_ERROR_MESSAGE
}

export function getStatusCode(error: AxiosError) {
  if (error.response) {
    return error.response.status
  }

  return 500
}
