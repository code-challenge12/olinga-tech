/* eslint-disable @typescript-eslint/no-explicit-any */

export function parseUrl(url: string, config: any = {}): string {
  const params = config.params

  if (params) {
    Object.entries(params).forEach(([key, value]: [string, any]) => {
      const regex = new RegExp(`:${key}`)
      if (regex.test(url)) {
        url = url.replace(`:${key}`, value)
        delete config.params[key]
      }
    })
  }

  return url
}
