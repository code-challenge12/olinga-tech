/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import axios, { AxiosResponse, AxiosError, AxiosRequestConfig } from 'axios'
import { parseUrl } from './helpers'

export enum Methods {
  get = 'get',
  post = 'post',
  put = 'put',
  patch = 'patch',
  delete = 'delete',
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Response = AxiosResponse<any>
export type NextFunction = () => Promise<AxiosResponse>
export type MiddleWareFunction = (
  config: AxiosRequestConfig,
  next: NextFunction,
) => Promise<AxiosResponse>
export type RequestGet<T = any> = (
  url: string,
  config?: AxiosRequestConfig,
  ...args: MiddleWareFunction[]
) => Promise<AxiosResponse<T>>
export type RequestPost<T = any> = (
  url: string,
  data: any,
  config?: AxiosRequestConfig,
  ...args: MiddleWareFunction[]
) => Promise<AxiosResponse<T>>
export type Interface<T = any> = (
  method: Methods,
  url: string,
  config?: AxiosRequestConfig,
  ...args: MiddleWareFunction[]
) => Promise<AxiosResponse<T>>

function errorHandler(_: AxiosError) {
  // if (error.response) {
  //   console.error(
  //     `${error.response.status}: ${error.response.data.description}`,
  //   )
  // } else if (typeof error === 'string' || typeof error === 'number') {
  //   console.error(`ERROR: ${error}`)
  // }
}

async function get<T = any>(
  url: string,
  config: AxiosRequestConfig = {},
  ...args: MiddleWareFunction[]
): Promise<AxiosResponse<T>> {
  try {
    switch (args.length) {
      case 0:
        url = parseUrl(url, config)
        return await axios.get(url, config)
      case 1:
      default:
        return args[0](
          config,
          async () => await get(url, config, ...args.slice(1)),
        )
    }
  } catch (error: any) {
    errorHandler(error)

    throw error
  }
}

async function post<T = any>(
  url: string,
  data: any,
  config: AxiosRequestConfig = {},
  ...args: MiddleWareFunction[]
): Promise<AxiosResponse<T>> {
  try {
    switch (args.length) {
      case 0:
        url = parseUrl(url, config)
        return await axios.post(url, data, config)
      case 1:
      default:
        return args[0](
          config,
          async () => await post(url, data, config, ...args.slice(1)),
        )
    }
  } catch (error: any) {
    errorHandler(error)

    throw error
  }
}

async function put<T = any>(
  url: string,
  data: any,
  config: AxiosRequestConfig = {},
  ...args: MiddleWareFunction[]
): Promise<AxiosResponse<T>> {
  try {
    switch (args.length) {
      case 0:
        url = parseUrl(url, config)
        return await axios.put(url, data, config)
      case 1:
      default:
        return args[0](
          config,
          async () => await put(url, data, config, ...args.slice(1)),
        )
    }
  } catch (error: any) {
    errorHandler(error)

    throw error
  }
}

async function patch<T = any>(
  url: string,
  data: any,
  config: AxiosRequestConfig = {},
  ...args: MiddleWareFunction[]
): Promise<AxiosResponse<T>> {
  try {
    switch (args.length) {
      case 0:
        url = parseUrl(url, config)
        return await axios.patch(url, data, config)
      case 1:
      default:
        return args[0](
          config,
          async () => await patch(url, data, config, ...args.slice(1)),
        )
    }
  } catch (error: any) {
    errorHandler(error)

    throw error
  }
}

async function remove<T = any>(
  url: string,
  config: AxiosRequestConfig = {},
  ...args: MiddleWareFunction[]
): Promise<AxiosResponse<T>> {
  try {
    switch (args.length) {
      case 0:
        url = parseUrl(url, config)
        return await axios.delete(url, config)
      case 1:
      default:
        return args[0](
          config,
          async () => await remove(url, config, ...args.slice(1)),
        )
    }
  } catch (error: any) {
    errorHandler(error)

    throw error
  }
}

async function requests<T = any>(
  method: Methods | string,
  url: string,
  config: AxiosRequestConfig = {},
  ...args: MiddleWareFunction[]
): Promise<AxiosResponse<T>> {
  switch (method) {
    case Methods.post:
      return post<T>(url, config.data, config, ...args)
    case Methods.put:
      return put<T>(url, config.data, config, ...args)
    case Methods.patch:
      return patch<T>(url, config.data, config, ...args)
    case Methods.delete:
      return remove<T>(url, config, ...args)
    case Methods.get:
    default:
      return get<T>(url, config, ...args)
  }
}

export default {
  requests,
  get,
  post,
  put,
  patch,
  delete: remove,
}
