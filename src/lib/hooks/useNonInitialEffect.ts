import { useEffect, useRef } from 'react'

export function useNonInitialEffect(
  effect: React.EffectCallback,
  deps: React.DependencyList,
) {
  const isInitRef = useRef(false)

  useEffect(() => {
    if (!isInitRef.current) {
      isInitRef.current = true
      return
    }

    return effect()
  }, deps)
}
