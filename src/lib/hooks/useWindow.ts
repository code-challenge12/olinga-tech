import { useEffect } from 'react'

export function useWindowResizeCallback(cb: React.EffectCallback) {
  useEffect(() => {
    window.addEventListener('resize', cb)

    return () => {
      window.removeEventListener('resize', cb)
    }
  }, [cb])
}
