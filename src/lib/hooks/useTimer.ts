import { useEffect } from 'react'

export function useInterval(
  effect: React.EffectCallback,
  period?: number | null,
  isRunning = true,
) {
  useEffect(() => {
    if (isRunning && period != null) {
      const timer = setInterval(effect, period)

      return () => clearInterval(timer)
    }
  }, [isRunning, period])
}

export function useTimeout(
  effect: React.EffectCallback,
  period?: number | null,
  isRunning = true,
) {
  useEffect(() => {
    if (isRunning && period != null) {
      const timer = setTimeout(effect, period)

      return () => clearTimeout(timer)
    }
  }, [isRunning, period])
}
