import { WithCSSVar } from '@chakra-ui/react'

export function getScaleValue(index: number) {
  return index === 0 ? 1 : index === 2 ? 1 / 3 : 2 / 3
}

export function getTransformValue(index: number) {
  return `translate3d(${-50 + (index === 0 ? 0 : index === 1 ? 80 : -80)}%,${
    -50 + (index === 0 ? -50 : index === 1 ? -30 : -20)
  }%,0) rotate(${
    index === 0 ? 0 : index === 1 ? 30 : -45
  }deg) scale(${getScaleValue(index)})`
}

export function getColor(isActive: boolean, theme: WithCSSVar<Dict>) {
  return {
    stroke: isActive
      ? `${theme.colors.icons.favorite}`
      : `${theme.colors.gray[700]}`,
    fill: isActive
      ? `${theme.colors.icons.favorite}FF`
      : `${theme.colors.icons.favorite}00`,
  }
}
