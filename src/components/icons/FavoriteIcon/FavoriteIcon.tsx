import { Center, Icon, SystemStyleObject, useTheme } from '@chakra-ui/react'
import { useSpring } from '@react-spring/web'
import { FiHeart } from 'react-icons/fi'

import { AnimatedBox, DEFAULT_SPRING_CONFIG } from '@/lib/animation'

import { FloatingFavoriteIcon } from './FloatingFavoriteIcon'
import { getColor } from './utils'

const floatingIconArray = new Array(3).fill(0)

type Props = {
  size?: number | string
  sx?: SystemStyleObject
  isActive?: boolean
}

export function FavoriteIcon({ isActive = false, size = '2.5em', sx }: Props) {
  const theme = useTheme()

  const [spring] = useSpring(
    () => ({
      ...getColor(isActive, theme),
      config: DEFAULT_SPRING_CONFIG,
    }),
    [isActive],
  )

  return (
    <Center sx={{ position: 'relative', boxSize: size, ...sx }}>
      <AnimatedBox
        style={spring}
        sx={{
          boxSize: '40%',
        }}
      >
        <Icon
          as={FiHeart}
          sx={{
            stroke: 'inherit',
            fill: 'inherit',
            boxSize: 'full',
          }}
        />
      </AnimatedBox>
      {floatingIconArray.map((_, index) => (
        <FloatingFavoriteIcon
          key={`${index}`}
          isActive={isActive}
          index={index}
        />
      ))}
    </Center>
  )
}
