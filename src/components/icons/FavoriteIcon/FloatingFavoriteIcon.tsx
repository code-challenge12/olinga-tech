import { useMemo, useRef, useEffect } from 'react'
import { Icon, useTheme } from '@chakra-ui/react'
import { useTransition } from '@react-spring/web'
import { FiHeart } from 'react-icons/fi'

import { AnimatedBox, DEFAULT_SPRING_CONFIG } from '@/lib/animation'

import { getScaleValue, getTransformValue, getColor } from './utils'

type Props = {
  index: number
  isActive?: boolean
}

export function FloatingFavoriteIcon({ isActive = false, index = 0 }: Props) {
  const theme = useTheme()

  const isMounted = useRef(false)

  useEffect(() => {
    isMounted.current = true
  }, [])

  const transitionsConfig = useMemo(() => {
    return {
      from: {
        opacity: 0,
        ...getColor(false, theme),
        transform: `translate3d(-50%, -50%, 0) rotate(0deg) scale(${getScaleValue(
          index,
        )})`,
      },
      enter: [
        ...(isMounted.current
          ? [
              {
                opacity: 1 - (index + 1) * 0.2,
                ...getColor(true, theme),
                transform: getTransformValue(index),
              },
            ]
          : []),
        { opacity: 0 },
      ],
      leave: { opacity: 0 },
      delay: (index + 1) * 100,
      config: DEFAULT_SPRING_CONFIG,
    }
  }, [index, isActive])

  const transitions = useTransition(
    isMounted.current && isActive,
    transitionsConfig,
  )

  return transitions((style, isActive) =>
    isActive ? (
      <AnimatedBox
        style={style}
        sx={{
          boxSize: '30%',
          position: 'absolute',
          top: '30%',
          left: '50%',
        }}
      >
        <Icon
          as={FiHeart}
          sx={{
            stroke: 'inherit',
            fill: 'inherit',
            boxSize: 'full',
          }}
        />
      </AnimatedBox>
    ) : null,
  )
}
