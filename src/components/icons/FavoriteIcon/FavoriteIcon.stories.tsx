import { useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { useInterval } from '@/lib/hooks'

import { FavoriteIcon } from './FavoriteIcon'

export default {
  title: 'Components/Icons/FavoriteIcon',
  component: FavoriteIcon,
  argTypes: {
    isActive: { control: 'boolean' },
  },
} as ComponentMeta<typeof FavoriteIcon>

const Template: ComponentStory<typeof FavoriteIcon> = (args) => {
  const [isActive, setActive] = useState(args.isActive)

  useInterval(() => {
    setActive((isActive) => !isActive)
  }, 2000)

  return <FavoriteIcon {...args} isActive={isActive} />
}

export const Default = Template.bind({})

Default.args = {
  isActive: false,
}
