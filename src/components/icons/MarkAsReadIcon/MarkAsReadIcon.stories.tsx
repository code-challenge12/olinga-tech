import { useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { useInterval } from '@/lib/hooks'

import { MarkAsReadIcon } from './MarkAsReadIcon'

export default {
  title: 'Components/Icons/MarkAsReadIcon',
  component: MarkAsReadIcon,
  argTypes: {
    isActive: { control: 'boolean' },
  },
} as ComponentMeta<typeof MarkAsReadIcon>

const Template: ComponentStory<typeof MarkAsReadIcon> = (args) => {
  const [isActive, setActive] = useState(args.isActive)

  useInterval(() => {
    setActive((isActive) => !isActive)
  }, 2000)

  return <MarkAsReadIcon {...args} isActive={isActive} />
}

export const Default = Template.bind({})

Default.args = {
  isActive: false,
}
