import { Box, Icon, SystemStyleObject } from '@chakra-ui/react'
import { IoBookOutline } from 'react-icons/io5'

type Props = {
  size?: number | string
  sx?: SystemStyleObject
  isActive?: boolean
}

export function MarkAsReadIcon({
  isActive = false,
  size = '2.5em',
  sx,
}: Props) {
  return (
    <Box sx={{ position: 'relative', boxSize: size, ...sx }}>
      <Icon
        as={IoBookOutline}
        sx={{
          boxSize: '40%',
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate3d(-50%, -50%, 0)',
          path: {
            transitionProperty: 'common',
            transitionDuration: 'slow',
            fill: isActive ? 'green.100' : 'transparent',
            stroke: isActive ? 'icons.markAsRead' : 'gray.700',
          },
        }}
      />
    </Box>
  )
}
