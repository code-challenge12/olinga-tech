import { Icon } from '@chakra-ui/react'
import { FiHeart, FiHome } from 'react-icons/fi'

import { NavItemLink } from './types'

export const NAV_LINKS: NavItemLink[] = [
  {
    to: 'home',
    label: 'Home',
    renderIcon: () => <Icon as={FiHome} />,
  },
  {
    to: 'favorite',
    label: 'My Fovorite',
    renderIcon: () => <Icon as={FiHeart} />,
  },
]
