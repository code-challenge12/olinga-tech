import { HStack } from '@chakra-ui/react'

import { NavItem } from './NavItem'

import { NavItemLink } from './types'

type Props = {
  links: NavItemLink[]
}

export function MenuDesktop({ links }: Props) {
  return (
    <HStack>
      {links.map((link, index) => {
        return <NavItem link={link} key={`${index}`} />
      })}
    </HStack>
  )
}
