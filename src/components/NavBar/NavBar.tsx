import { HStack, Show, Center, Spacer } from '@chakra-ui/react'

import { MenuDesktop } from './MenuDesktop'
import { MenuMobile } from './MenuMobile'
import { SearchField } from './SearchField'

import { NAV_LINKS } from './constants'

type Props = {
  defaultSearchValue?: string
  onSearch?: (value: string) => void
}

export function NavBar({ onSearch, defaultSearchValue }: Props) {
  return (
    <Center
      sx={{
        zIndex: 'sticky',
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        height: {
          base: 'navBarHeight.base',
          md: 'navBarHeight.md',
        },
        boxShadow: 'md',
        background: 'white',
      }}
    >
      <HStack layerStyle="container">
        <Show below="md">
          <MenuMobile links={NAV_LINKS} />
        </Show>
        <Show above="md">
          <MenuDesktop links={NAV_LINKS} />
        </Show>
        <Spacer />
        {onSearch ? (
          <SearchField onSubmit={onSearch} defaultValue={defaultSearchValue} />
        ) : (
          <Spacer />
        )}
      </HStack>
    </Center>
  )
}
