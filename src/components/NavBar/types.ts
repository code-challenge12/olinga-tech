import { RouteKey } from '@/lib/link'

export type NavItemLink = {
  label: React.ReactNode
  renderIcon: (isActive?: boolean) => React.ReactElement
} & RequireAtLeastOne<{
  to: RouteKey
  href: string
}>
