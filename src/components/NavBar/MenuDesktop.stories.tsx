import { ComponentStory, ComponentMeta } from '@storybook/react'

import { MenuDesktop } from './MenuDesktop'

import { NAV_LINKS } from './constants'

export default {
  title: 'Components/NavBar/MenuDesktop',
  component: MenuDesktop,
  argTypes: {},
} as ComponentMeta<typeof MenuDesktop>

const Template: ComponentStory<typeof MenuDesktop> = (args) => {
  return <MenuDesktop {...args} />
}

export const Default = Template.bind({})

Default.args = {
  links: NAV_LINKS,
}
