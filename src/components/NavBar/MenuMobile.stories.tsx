import { ComponentStory, ComponentMeta } from '@storybook/react'

import { MenuMobile } from './MenuMobile'

import { NAV_LINKS } from './constants'

export default {
  title: 'Components/NavBar/MenuMobile',
  component: MenuMobile,
  argTypes: {},
} as ComponentMeta<typeof MenuMobile>

const Template: ComponentStory<typeof MenuMobile> = (args) => {
  return <MenuMobile {...args} />
}

export const Default = Template.bind({})

Default.args = {
  links: NAV_LINKS,
}
