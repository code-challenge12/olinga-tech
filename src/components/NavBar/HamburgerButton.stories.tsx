import { useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { useInterval } from '@/lib/hooks'

import { HamburgerButton } from './HamburgerButton'

export default {
  title: 'Components/NavBar/HamburgerButton',
  component: HamburgerButton,
  argTypes: {
    isActive: { control: 'boolean' },
  },
} as ComponentMeta<typeof HamburgerButton>

const Template: ComponentStory<typeof HamburgerButton> = (args) => {
  const [isActive, setActive] = useState(args.isActive)

  useInterval(() => {
    setActive((isActive) => !isActive)
  }, 2000)

  return <HamburgerButton isActive={isActive} />
}

export const Default = Template.bind({})

Default.args = {
  isActive: false,
}
