import { useTrail } from '@react-spring/web'
import { Icon, IconButton, IconButtonProps } from '@chakra-ui/react'

import { AnimatedBox, DEFAULT_SPRING_CONFIG } from '@/lib/animation'

type Props = {
  isActive?: boolean
}

function HamburgerIcon({ isActive = false }: Props) {
  const [trails] = useTrail(
    4,
    () => {
      const styles = {
        opacity: isActive ? 0 : 1,
        transform: isActive ? 'translateX(-100%)' : 'translateX(0%)',
      }
      return {
        from: styles,
        to: styles,
        config: DEFAULT_SPRING_CONFIG,
      }
    },
    [isActive],
  )

  return (
    <svg
      width="1.5em"
      height="1.5em"
      viewBox="0 0 16 16"
      // style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"
    >
      <AnimatedBox
        as="path"
        style={trails[0]}
        d="M2,12.5c0,-0.274 0.226,-0.5 0.5,-0.5l7,0c0.274,0 0.5,0.226 0.5,0.5c0,0.274 -0.226,0.5 -0.5,0.5l-7,0c-0.274,0 -0.5,-0.226 -0.5,-0.5Z"
      />
      <AnimatedBox
        as="path"
        style={trails[1]}
        d="M2,9.5c0,-0.274 0.226,-0.5 0.5,-0.5l11,0c0.274,0 0.5,0.226 0.5,0.5c0,0.274 -0.226,0.5 -0.5,0.5l-11,0c-0.274,0 -0.5,-0.226 -0.5,-0.5Z"
      />
      <AnimatedBox
        as="path"
        style={trails[2]}
        d="M2,6.5c0,-0.274 0.226,-0.5 0.5,-0.5l11,0c0.274,0 0.5,0.226 0.5,0.5c0,0.274 -0.226,0.5 -0.5,0.5l-11,0c-0.274,0 -0.5,-0.226 -0.5,-0.5Z"
      />
      <AnimatedBox
        as="path"
        style={trails[3]}
        d="M6,3.5c0,-0.274 0.226,-0.5 0.5,-0.5l7,0c0.274,0 0.5,0.226 0.5,0.5c0,0.274 -0.226,0.5 -0.5,0.5l-7,0c-0.274,0 -0.5,-0.226 -0.5,-0.5Z"
      />
    </svg>
  )
}

export function HamburgerButton({
  isActive = false,
  sx,
  ...rest
}: Props & Partial<IconButtonProps>) {
  return (
    <IconButton
      variant="ghost"
      colorScheme="gray"
      icon={
        <Icon
          as={HamburgerIcon}
          isActive={isActive}
          sx={{ fontSize: 'inherit' }}
        />
      }
      size={{
        base: 'md',
        md: 'lg',
      }}
      sx={{
        borderRadius: 'sm',
        padding: '0 !important',
        minW: 'unset',
        ...sx,
      }}
      {...rest}
      aria-label={rest['aria-label'] ?? 'menu'}
    />
  )
}
