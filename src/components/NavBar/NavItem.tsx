import { Button, HStack, SystemStyleObject } from '@chakra-ui/react'

import { Link, useActiveRoute } from '@/lib/link'

import { NavItemLink } from './types'

type Props = {
  link: NavItemLink
  sx?: SystemStyleObject
  onClick?: () => void
}

export function NavItem({
  link: { label, renderIcon, ...rest },
  sx,
  onClick,
}: Props) {
  const checkActiveRoute = useActiveRoute()

  const { to } = rest
  const isActive = to ? checkActiveRoute(to) : false

  return (
    <Link {...rest} onClick={onClick}>
      <Button
        as={HStack}
        leftIcon={renderIcon(isActive)}
        variant={isActive ? 'outline' : 'ghost'}
        colorScheme={isActive ? 'primary' : 'gray'}
        sx={{
          borderRadius: 'sm',
          width: 'full',
          justifyContent: 'flex-start',
          ...sx,
        }}
      >
        {label}
      </Button>
    </Link>
  )
}
