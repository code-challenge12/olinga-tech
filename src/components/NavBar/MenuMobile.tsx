import { Stack, DrawerBody, useDisclosure } from '@chakra-ui/react'

import { Drawer } from '@/components/Drawer'

import { NavItem } from './NavItem'
import { HamburgerButton } from './HamburgerButton'

import { NavItemLink } from './types'

type Props = {
  links: NavItemLink[]
}

export function MenuMobile({ links }: Props) {
  const { isOpen, onClose, getButtonProps } = useDisclosure()

  return (
    <>
      <HamburgerButton isActive={isOpen} {...getButtonProps()} />

      <Drawer isOpen={isOpen} onClose={onClose}>
        <DrawerBody sx={{ pt: 16 }}>
          <Stack>
            {links.map((link, index) => {
              return <NavItem key={`${index}`} link={link} onClick={onClose} />
            })}
          </Stack>
        </DrawerBody>
      </Drawer>
    </>
  )
}
