import { useState, useRef, useTransition } from 'react'
import { FiSearch } from 'react-icons/fi'
import { useForm, Controller } from 'react-hook-form'
import { HStack, Input, Box, Button, Icon, useToken } from '@chakra-ui/react'

import { useNonInitialEffect } from '@/lib/hooks'

type Props = {
  defaultValue?: string
  value?: string
  onSubmit?: (value: string) => void | Promise<void>
}

type FormValue = {
  search: string
}

export function SearchField({ onSubmit, defaultValue = '', value }: Props) {
  const [_, startTransition] = useTransition()
  const transitionDuration = useToken('transition.duration', 'normal')

  const [isFocused, setFocus] = useState(false)
  const searchRef = useRef<HTMLInputElement | null>(null)

  const methods = useForm<FormValue>({
    mode: 'onBlur',
    defaultValues: { search: defaultValue },
    ...(typeof value !== 'undefined' && {
      values: {
        search: value,
      },
    }),
  })

  const {
    handleSubmit,
    control,
    reset,
    formState: { isSubmitting },
  } = methods

  const _onSubmit = handleSubmit((data: FormValue) => {
    if (isSubmitting || !onSubmit) return

    onSubmit(data.search)

    onBlur()
  })

  const onFocus = () => {
    setFocus(true)

    searchRef.current?.focus()
  }

  const onBlur = () => {
    setFocus(false)

    searchRef.current?.blur()
  }

  useNonInitialEffect(() => {
    startTransition(() => reset({ search: defaultValue }))
  }, [defaultValue])

  return (
    <form onSubmit={_onSubmit}>
      <Controller
        control={control}
        name="search"
        render={({ field: { value, onChange } }) => {
          const isShowInput = Boolean(value) || isFocused

          return (
            <>
              <Box
                sx={{
                  position: 'fixed',
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  bgColor: 'blackAlpha.500',
                  zIndex: 'overlay',
                  height: '100vh',
                  ...(isFocused
                    ? {
                        visibility: 'visible',
                        opacity: 1,
                        transition: `opacity ${transitionDuration}, visibility 0ms 0ms`,
                      }
                    : {
                        visibility: 'hidden',
                        opacity: 0,
                        transition: `opacity ${transitionDuration}, visibility 0ms ${transitionDuration}`,
                      }),
                }}
              />
              <Button
                as={HStack}
                spacing="0"
                iconSpacing="0"
                size={{
                  base: 'md',
                  md: 'lg',
                }}
                tabIndex={-1}
                leftIcon={<Icon as={FiSearch} sx={{ fontSize: '1.2em' }} />}
                colorScheme="gray"
                onFocus={onFocus}
                onBlur={onBlur}
                sx={{
                  transitionDuration,
                  cursor: 'pointer',
                  borderRadius: 'sm',
                  overflow: 'hidden',
                  zIndex: 'modal',
                  color: 'inherit',

                  ...(isShowInput
                    ? {
                        transitionProperty: 'common',
                        backgroundColor: 'white',
                        _hover: {
                          backgroundColor: 'white',
                        },
                      }
                    : {
                        transitionProperty: 'none',
                        backgroundColor: 'transparent',
                      }),

                  ...(isFocused
                    ? {
                        boxShadow: 'md',
                      }
                    : {}),
                  _focus: {},
                  _active: {},
                }}
              >
                <Input
                  onFocus={onFocus}
                  onBlur={() => {
                    onBlur()

                    _onSubmit()
                  }}
                  ref={searchRef}
                  onChange={onChange}
                  onKeyDown={(event) => {
                    switch (event.key) {
                      case 'Escape':
                        searchRef.current?.blur()
                        break
                    }
                  }}
                  value={value}
                  focusBorderColor="gray.400"
                  variant="flushed"
                  size={{
                    base: 'sm',
                    sm: 'md',
                  }}
                  sx={{
                    transitionDuration,
                    width: '280px',
                    borderRadius: 'none !important',
                    outline: 'none !important',
                    boxShadow: 'none !important',
                    ...(isShowInput
                      ? {
                          transitionProperty: 'opacity, transform, max-width',
                          maxWidth: { base: '200px', sm: '280px' },
                          opacity: 1,
                          padding: 2,
                        }
                      : {
                          transitionProperty: 'none',
                          maxWidth: '0px',
                          opacity: 0,
                          padding: '0 !important',
                        }),
                  }}
                />
              </Button>
            </>
          )
        }}
      />
    </form>
  )
}
