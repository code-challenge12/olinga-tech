import { ComponentStory, ComponentMeta } from '@storybook/react'

import { NavBar } from './NavBar'

export default {
  title: 'Components/NavBar',
  component: NavBar,
  argTypes: {},
} as ComponentMeta<typeof NavBar>

const Template: ComponentStory<typeof NavBar> = (args) => {
  return <NavBar {...args} />
}

export const Default = Template.bind({})

Default.args = {}
