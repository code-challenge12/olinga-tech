import { ComponentStory, ComponentMeta } from '@storybook/react'

import { NoData } from './NoData'

export default {
  title: 'Components/NoData',
  component: NoData,
  argTypes: {},
} as ComponentMeta<typeof NoData>

const Template: ComponentStory<typeof NoData> = (args) => {
  return <NoData />
}

export const Default = Template.bind({})

Default.args = {}
