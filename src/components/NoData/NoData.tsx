import { Text, Center } from '@chakra-ui/react'

export function NoData() {
  return (
    <Center>
      <Text sx={{ textAlign: 'center' }}>Nothing found</Text>
    </Center>
  )
}
