import { ComponentStory, ComponentMeta } from '@storybook/react'

import { CopyButton } from './CopyButton'

export default {
  title: 'Components/CopyButton',
  component: CopyButton,
  argTypes: {
    clipboardContent: { control: 'text' },
  },
} as ComponentMeta<typeof CopyButton>

const Template: ComponentStory<typeof CopyButton> = (args) => {
  return <CopyButton {...args} />
}

export const Default = Template.bind({})

Default.args = {
  clipboardContent:
    'Lorem ipsum elit duis ex exercitation labore pariatur do et fugiat consectetur sint minim quis exercitation minim.',
}
