import {
  Tooltip,
  IconButton,
  Icon,
  IconButtonProps,
  useClipboard,
} from '@chakra-ui/react'
import { FiCopy } from 'react-icons/fi'

type Props = {
  clipboardContent: string
} & Partial<IconButtonProps>

export function CopyButton({ clipboardContent, ...rest }: Props) {
  const { onCopy, hasCopied } = useClipboard(clipboardContent)

  return (
    <Tooltip isOpen={hasCopied} label="Copied">
      <IconButton
        icon={<Icon as={FiCopy} />}
        variant="ghost"
        colorScheme="blackAlpha"
        {...rest}
        aria-label={rest['aria-label'] ?? 'copy'}
        onClick={onCopy}
      />
    </Tooltip>
  )
}
