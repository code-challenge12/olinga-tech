import { useTransition } from '@react-spring/web'
import { DEFAULT_SPRING_CONFIG, AnimatedBox } from '@/lib/animation'

export function PageTransition({ children }: React.PropsWithChildren) {
  const transitions = useTransition(children, {
    from: {
      opacity: 0,
    },
    enter: {
      opacity: 1,
    },
    leave: {
      opacity: 0,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    config: DEFAULT_SPRING_CONFIG,
  })

  return transitions((style, children) => (
    <AnimatedBox style={style}>{children}</AnimatedBox>
  ))
}
