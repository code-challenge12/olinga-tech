import { Button, Center } from '@chakra-ui/react'
import { useRouter } from 'next/router'

export function BackButton() {
  const router = useRouter()

  if (!document || document.referrer) return null

  return (
    <Center>
      <Button
        colorScheme="blackAlpha"
        variant="link"
        onClick={router.back}
        sx={{
          mt: 10,
          minW: '10vw',
          maxW: '100px',
          color: 'gray.800',
          fontWeight: 'normal',
        }}
      >
        Back
      </Button>
    </Center>
  )
}
