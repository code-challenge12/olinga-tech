import {
  Drawer as ChakraDrawer,
  DrawerOverlay,
  DrawerContent,
  DrawerProps as ChakraDrawerProps,
  DrawerCloseButton,
  SystemStyleObject,
} from '@chakra-ui/react'

export type DrawerProps = {
  isOpen: boolean
  onClose: () => void
  children: React.ReactNode
  withCloseButton?: boolean
  DrawerContentSx?: SystemStyleObject
} & ChakraDrawerProps

export const Drawer = (props: DrawerProps) => {
  const {
    isOpen,
    onClose,
    children,
    withCloseButton = true,
    DrawerContentSx,
    placement = 'left',
    ...rest
  } = props

  return (
    <ChakraDrawer
      onClose={onClose}
      isOpen={isOpen}
      placement={placement}
      {...rest}
    >
      <DrawerOverlay data-testid="Drawer-overlay">
        <DrawerContent pb="4" data-testid="Drawer-content" sx={DrawerContentSx}>
          {children}

          {withCloseButton && (
            <DrawerCloseButton data-testid="drawer-close-button" />
          )}
        </DrawerContent>
      </DrawerOverlay>
    </ChakraDrawer>
  )
}
