import { Progress, VStack, Text, SystemStyleObject } from '@chakra-ui/react'

type LoadingProps = {
  size?: string
  sx?: SystemStyleObject
  withLabel?: boolean
}

export function Loading({ size = 'xs', sx, withLabel = true }: LoadingProps) {
  return (
    <VStack sx={{ minW: '280px', color: 'gray.700', ...sx }}>
      <Progress
        isIndeterminate
        colorScheme="primary"
        size={size}
        sx={{ width: 'full' }}
      />
      {withLabel && <Text>Loading</Text>}
    </VStack>
  )
}

type RenderDataProps = {
  isLoading?: boolean
  render: () => React.ReactElement
}

export function RenderData({ render, isLoading }: RenderDataProps) {
  if (isLoading) {
    return <Loading />
  }

  return render()
}
