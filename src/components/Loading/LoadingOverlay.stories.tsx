import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Image } from '@chakra-ui/react'

import { LoadingOverlay } from './LoadingOverlay'

export default {
  title: 'Components/Loading/LoadingOverlay',
  component: LoadingOverlay,
  argTypes: {
    isLoading: { control: 'boolean' },
  },
} as ComponentMeta<typeof LoadingOverlay>

const Template: ComponentStory<typeof LoadingOverlay> = (args) => {
  return (
    <LoadingOverlay {...args}>
      <Image
        src="https://images.dog.ceo/breeds/bullterrier-staffordshire/n02093256_8205.jpg"
        alt=""
      />
    </LoadingOverlay>
  )
}

export const Default = Template.bind({})

Default.args = {
  isLoading: false,
}
