import { Box, Center } from '@chakra-ui/react'
import { useTransition } from '@react-spring/web'

import { DEFAULT_SPRING_CONFIG, AnimatedBox } from '@/lib/animation'

import { Loading } from './Loading'

type Props = {
  isLoading: boolean
  size?: string
  children?: React.ReactNode
}

export function LoadingOverlay({ isLoading, size, children }: Props) {
  const transitions = useTransition(isLoading, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
    config: DEFAULT_SPRING_CONFIG,
  })

  return (
    <Box
      sx={{
        position: 'relative',
      }}
    >
      {children}
      {transitions((spring, isLoading) => {
        return isLoading ? (
          <AnimatedBox
            as={Center}
            style={spring}
            sx={{
              position: 'absolute',
              boxSize: 'full',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              cursor: 'not-allowed',
            }}
          >
            <Box
              sx={{
                position: 'absolute',
                boxSize: 'full',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                backgroundColor: 'whiteAlpha.600',
              }}
            />
            <Loading
              withLabel={false}
              size={size}
              sx={{
                alignSelf: 'flex-start',
                marginTop: '200px',
              }}
            />
          </AnimatedBox>
        ) : null
      })}
    </Box>
  )
}
