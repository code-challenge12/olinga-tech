export type Link = {
  pathname: string
  hash?: string
  query?: Dict
  locales?: string[]
  isExternal?: boolean
  priority?: number
}
