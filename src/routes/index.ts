export * from './types'

export const home = {
  pathname: '/',
}

export const favorite = {
  pathname: '/favorite',
}

export const details = {
  pathname: '/volumes/[id]',
}
