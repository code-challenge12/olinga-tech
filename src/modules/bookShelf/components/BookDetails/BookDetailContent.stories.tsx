import { ComponentStory, ComponentMeta } from '@storybook/react'
import { useState } from 'react'

import { useInterval } from '@/lib/hooks'

import { BookItem as BookItemType } from '@/modules/bookShelf/types'
import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'

import { BookDetailsContent } from './BookDetailsContent'

export default {
  title: 'Modules/BookShelf/Components/BookDetails/Content',
  component: BookDetailsContent,
  argTypes: {},
} as ComponentMeta<typeof BookDetailsContent>

const Template: ComponentStory<typeof BookDetailsContent> = (args) => {
  const [isFavorite, setFavorite] = useState(false)

  useInterval(() => {
    setFavorite((isFavorite) => !isFavorite)
  }, 2000)

  return <BookDetailsContent {...args} isFavorite={isFavorite} />
}

export const Default = Template.bind({})

Default.args = {
  bookData: __MOCK_BOOK_ITEM as BookItemType,
}
