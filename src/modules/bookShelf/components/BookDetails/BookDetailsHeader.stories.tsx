import { ComponentStory, ComponentMeta } from '@storybook/react'
import { useState } from 'react'

import { useInterval } from '@/lib/hooks'

import { BookItem as BookItemType } from '@/modules/bookShelf/types'
import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'

import { BookDetailsHeader } from './BookDetailsHeader'

export default {
  title: 'Modules/BookShelf/Components/BookDetails/Header',
  component: BookDetailsHeader,
  argTypes: {
    isLoading: { control: 'boolean' },
  },
} as ComponentMeta<typeof BookDetailsHeader>

const Template: ComponentStory<typeof BookDetailsHeader> = (args) => {
  const [isFavorite, setFavorite] = useState(false)

  useInterval(() => {
    setFavorite((isFavorite) => !isFavorite)
  }, 2000)

  return <BookDetailsHeader {...args} isFavorite={isFavorite} />
}

export const Default = Template.bind({})

Default.args = {
  bookData: __MOCK_BOOK_ITEM as BookItemType,
}
