import { Stack, SimpleGrid, Box, Divider, Text, Icon } from '@chakra-ui/react'
import { RiDoubleQuotesL } from 'react-icons/ri'

import { ButtonGroup } from '@/modules/bookShelf/components/ButtonGroup'
import { Price, Rating } from '@/modules/bookShelf/components/fields'
import { BookItem as BookItemType } from '@/modules/bookShelf/types'

import { getDateString } from '@/lib/utils'

const DIVIDER_SX = {
  gridColumn: '1 / 3',
}

type FieldProps = {
  label: React.ReactNode
  content: React.ReactNode
}

function Field({ label, content }: FieldProps) {
  return (
    <>
      <Box sx={{ py: 4, fontWeight: 'medium' }}>{label} :</Box>
      <Box sx={{ py: 4 }}>{content}</Box>
    </>
  )
}

type Props = {
  bookData: BookItemType
  onClickFavorite?: () => void
  onClickMarkAsRead?: () => void
  isFavorite?: boolean
  haveRead?: boolean
}

export function BookDetailsContent({
  bookData,
  onClickFavorite,
  onClickMarkAsRead,
  isFavorite,
  haveRead,
}: Props) {
  const { volumeInfo, saleInfo } = bookData

  return (
    <Stack
      layerStyle="container"
      spacing="6"
      sx={{
        fontSize: { base: 'xs', xs: 'sm', sm: 'md' },
        pb: { base: 20, sm: 40 },
        pt: { base: 10, sm: 20 },
      }}
    >
      <Box sx={{ position: 'relative' }}>
        <Icon
          as={RiDoubleQuotesL}
          sx={{
            position: 'absolute',
            top: 0,
            left: 0,
            transform: 'translateX(-100%) translateY(-70%)',
            fontSize: { base: '1em', sm: '2em' },
          }}
        />
        <Text
          sx={{ textAlign: 'justify' }}
          dangerouslySetInnerHTML={{ __html: volumeInfo.description }}
        />
      </Box>
      <SimpleGrid
        templateColumns={{ base: '1fr 1fr', sm: '1fr 3fr' }}
        columnGap="2"
        rowGap="1"
      >
        {volumeInfo.industryIdentifiers.map((isbn) => {
          return (
            <Field
              key={isbn.type}
              label={isbn.type}
              content={isbn.identifier}
            />
          )
        })}
        <Divider sx={DIVIDER_SX} />
        <Field label="Rating" content={<Rating {...volumeInfo} />} />
        <Divider sx={DIVIDER_SX} />
        <Field label="Total Pages" content={volumeInfo.pageCount} />
        <Divider sx={DIVIDER_SX} />
        <Field label="Publisher" content={volumeInfo.publisher} />
        <Divider sx={DIVIDER_SX} />
        <Field
          label="Published Date"
          content={getDateString(volumeInfo.publishedDate)}
        />
        <Divider sx={DIVIDER_SX} />
        <Field label="Price" content={<Price {...saleInfo} />} />
        <Divider sx={DIVIDER_SX} />
        <ButtonGroup
          isFavorite={isFavorite}
          haveRead={haveRead}
          previewLink={volumeInfo.previewLink}
          buyLink={saleInfo.buyLink}
          onClickFavorite={onClickFavorite}
          onClickMarkAsRead={onClickMarkAsRead}
          sx={{ ...DIVIDER_SX, justifyContent: 'center', pt: 10 }}
        />
      </SimpleGrid>
    </Stack>
  )
}
