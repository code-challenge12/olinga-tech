import {
  VStack,
  Image,
  HStack,
  Text,
  Spacer,
  Divider,
  Box,
} from '@chakra-ui/react'

import { CopyButton } from '@/components/CopyButton'

import { Price } from '@/modules/bookShelf/components/fields'
import { getImageSrc } from '@/modules/bookShelf/utils'
import { BookItem as BookItemType } from '@/modules/bookShelf/types'

import { ButtonGroup } from '@/modules/bookShelf/components/ButtonGroup'

const IMAGE_WIDTH = {
  base: '240px',
  sm: '320px',
}

const imageHeight = {
  base: '320px',
  sm: '480px',
}

const imageSx = {
  maxWidth: IMAGE_WIDTH,
  height: imageHeight,
  objectFit: 'contain',
  filter: 'drop-shadow(5px 5px 10px rgba(0, 0, 0, 0.2))',
  objectPosition: 'bottom',
}

type Props = {
  bookData: BookItemType
  onClickFavorite?: () => void
  onClickMarkAsRead?: () => void
  isFavorite?: boolean
  haveRead?: boolean
}

export function BookDetailsHeader({
  bookData,
  onClickFavorite,
  onClickMarkAsRead,
  isFavorite,
  haveRead,
}: Props) {
  const { volumeInfo, saleInfo } = bookData
  const { title, previewLink, authors } = volumeInfo
  const { buyLink } = saleInfo

  return (
    <VStack
      layerStyle="container"
      spacing={{ base: '-40px', sm: '-100px' }}
      sx={{
        fontSize: { base: 'sm', sm: 'md' },
        textAlign: 'center',
      }}
    >
      <Image
        {...getImageSrc(bookData, { width: 600, height: 1000 })}
        fallback={<Box sx={{ ...imageSx, bgColor: 'whiteAlpha.500' }} />}
        alt={title}
        placeholder="Cover Image"
        sx={imageSx}
      />
      <VStack
        sx={{
          backgroundColor: 'white',
          borderRadius: 'md',
          padding: {
            base: 4,
            sm: 8,
          },
          paddingTop: { base: '80px', sm: '140px' },
          boxShadow: 'lg',
          w: 'full',
        }}
      >
        <HStack sx={{ alignItems: 'flex-start' }}>
          <Text
            sx={{ fontWeight: 'medium', fontSize: { base: 'lg', sm: '2xl' } }}
          >
            {title}
          </Text>
          <CopyButton clipboardContent={title} />
        </HStack>

        <Text>
          By{' '}
          <Text as="span" sx={{ fontStyle: 'italic' }}>
            {authors?.join(' & ') ?? '-'}
          </Text>
        </Text>

        <Divider />
        <Price {...saleInfo} />
        <Spacer />
        <ButtonGroup
          previewLink={previewLink}
          buyLink={buyLink}
          isFavorite={isFavorite}
          haveRead={haveRead}
          onClickFavorite={onClickFavorite}
          onClickMarkAsRead={onClickMarkAsRead}
        />
      </VStack>
    </VStack>
  )
}
