import { ComponentStory, ComponentMeta } from '@storybook/react'

import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'

import { ButtonGroup } from './ButtonGroup'

export default {
  title: 'Modules/BookShelf/Components/ButtonGroup',
  component: ButtonGroup,
  argTypes: {
    isFavorite: { control: 'boolean' },
    haveRead: { control: 'boolean' },
  },
} as ComponentMeta<typeof ButtonGroup>

const Template: ComponentStory<typeof ButtonGroup> = (args) => {
  return <ButtonGroup {...args} />
}

export const Default = Template.bind({})

Default.args = {
  isFavorite: false,
  haveRead: false,
  previewLink: __MOCK_BOOK_ITEM.volumeInfo.previewLink,
  buyLink: __MOCK_BOOK_ITEM.saleInfo.buyLink,
}
