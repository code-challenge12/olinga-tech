import {
  IconButton,
  Icon,
  HStack,
  Tooltip,
  ButtonGroup as CKButtonGroup,
  SystemStyleObject,
} from '@chakra-ui/react'
import { FiShoppingCart } from 'react-icons/fi'
import { AiOutlineFileSearch } from 'react-icons/ai'

import { FavoriteIcon, MarkAsReadIcon } from '@/components/icons'
import { Link } from '@/lib/link'

import { VolumeInfo, SaleInfo } from '@/modules/bookShelf/types'

type Props = {
  onClickFavorite?: () => void
  onClickMarkAsRead?: () => void
  previewLink?: VolumeInfo['previewLink']
  buyLink?: SaleInfo['buyLink']
  isFavorite?: boolean
  haveRead?: boolean
  sx?: SystemStyleObject
}

export function ButtonGroup({
  onClickFavorite,
  onClickMarkAsRead,
  previewLink,
  buyLink,
  isFavorite,
  haveRead,
  sx,
}: Props) {
  return (
    <CKButtonGroup as={HStack} sx={sx} colorScheme="blackAlpha" variant="ghost">
      <Tooltip label="Add to favorite">
        <IconButton
          aria-label="add to favorite"
          onClick={onClickFavorite}
          icon={<FavoriteIcon isActive={isFavorite} />}
        />
      </Tooltip>
      <Tooltip label="Mark as read">
        <IconButton
          aria-label="mark as read"
          onClick={onClickMarkAsRead}
          icon={<MarkAsReadIcon isActive={haveRead} />}
        />
      </Tooltip>
      {previewLink && (
        <Link href={previewLink}>
          <Tooltip label="Preview">
            <IconButton
              aria-label="Preview"
              icon={<Icon as={AiOutlineFileSearch} color="gray.700" />}
            />
          </Tooltip>
        </Link>
      )}
      {buyLink && (
        <Link href={buyLink}>
          <Tooltip label="Buy">
            <IconButton
              aria-label="Buy"
              icon={<Icon as={FiShoppingCart} color="gray.700" />}
            />
          </Tooltip>
        </Link>
      )}
    </CKButtonGroup>
  )
}
