import { ComponentStory, ComponentMeta } from '@storybook/react'

import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'
import { SaleInfo } from '@/modules/bookShelf/types'

import { Price } from './Price'

export default {
  title: 'Modules/BookShelf/Components/Fields/Price',
  component: Price,
  argTypes: {},
} as ComponentMeta<typeof Price>

const Template: ComponentStory<typeof Price> = (args) => {
  return <Price {...args} />
}

export const Default = Template.bind({})

Default.args = {
  saleability: __MOCK_BOOK_ITEM.saleInfo.saleability as SaleInfo['saleability'],
  retailPrice: __MOCK_BOOK_ITEM.saleInfo.retailPrice,
  listPrice: __MOCK_BOOK_ITEM.saleInfo.listPrice,
}
