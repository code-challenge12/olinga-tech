import { render, waitFor, screen } from 'test-utils'

import { Price, Props as PriceProps } from './Price'

const renderPrice = async (props: PriceProps) => {
  const utils = render(<Price {...props} />)

  const retailPriceElement = utils.getByTestId('retail-price')

  return {
    ...utils,
    retailPriceElement,
  }
}

describe('Price', () => {
  it('should render correctly', async () => {
    const { retailPriceElement } = await renderPrice({
      saleability: 'FOR_SALE',
      retailPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
      listPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
    })

    expect(retailPriceElement).toBeInTheDocument()
  })

  it('should display FREE label', async () => {
    await renderPrice({
      saleability: 'FREE',
      retailPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
      listPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
    })

    await waitFor(() => {
      expect(screen.getByText('FREE')).toBeInTheDocument()
    })
  })

  it('should display "Not for sale" label', async () => {
    await renderPrice({
      saleability: 'NOT_FOR_SALE',
      retailPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
      listPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
    })

    await waitFor(() => {
      expect(screen.getByText('Not for sale')).toBeInTheDocument()
    })
  })

  it('should display listPrice', async () => {
    const { retailPriceElement, getByTestId } = await renderPrice({
      saleability: 'FOR_SALE',
      retailPrice: {
        currencyCode: 'USD',
        amount: 100,
      },
      listPrice: {
        currencyCode: 'USD',
        amount: 80,
      },
    })

    const listingPriceElement = getByTestId('listing-price')

    await waitFor(() => {
      expect(retailPriceElement).toBeInTheDocument()
      expect(listingPriceElement).toBeInTheDocument()
    })
  })
})
