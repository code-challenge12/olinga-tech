import { Text, SystemStyleObject } from '@chakra-ui/react'

import { getLocaleNumberString } from '@/lib/utils'
import { SaleInfo } from '@/modules/bookShelf/types'
import { comparePrice } from '@/modules/bookShelf/utils'

export type Props = Pick<
  SaleInfo,
  'saleability' | 'retailPrice' | 'listPrice'
> & {
  sx?: SystemStyleObject
}

export function Price({ saleability, retailPrice, listPrice, sx }: Props) {
  return (
    <Text sx={sx} data-testid="retail-price">
      {saleability === 'FREE' ? (
        'FREE'
      ) : saleability === 'NOT_FOR_SALE' ? (
        'Not for sale'
      ) : retailPrice && listPrice ? (
        <>
          {getLocaleNumberString(
            retailPrice.amount,
            {
              style: 'currency',
              currency: retailPrice.currencyCode,
            },
            'en-US',
          )}
          {!comparePrice(retailPrice, listPrice) && (
            <Text
              as="span"
              data-testid="listing-price"
              sx={{
                textDecoration: 'line-through',
                fontSize: 'xs',
                color: 'gray.600',
                ml: 1,
              }}
            >
              {getLocaleNumberString(
                listPrice.amount,
                {
                  style: 'currency',
                  currency: listPrice.currencyCode,
                },
                'en-US',
              )}
            </Text>
          )}
        </>
      ) : null}
    </Text>
  )
}
