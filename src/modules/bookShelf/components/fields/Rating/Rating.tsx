import { Text, SystemStyleObject } from '@chakra-ui/react'

import { VolumeInfo } from '@/modules/bookShelf/types'
import { getLocaleNumberString } from '@/lib/utils'

type Props = Pick<VolumeInfo, 'averageRating' | 'ratingsCount'> & {
  sx?: SystemStyleObject
}

export function Rating({ averageRating, ratingsCount, sx }: Props) {
  return (
    <Text sx={sx}>
      {typeof averageRating !== 'undefined' ? (
        <>
          {getLocaleNumberString(averageRating, {
            minimumFractionDigits: 0,
            maximumFractionDigits: 2,
          })}
          /5
          <Text
            as="span"
            sx={{
              fontSize: 'xs',
              color: 'gray.600',
              ml: 1,
            }}
          >
            (
            {getLocaleNumberString(ratingsCount ?? 0, {
              minimumFractionDigits: 0,
              maximumFractionDigits: 0,
            })}
            )
          </Text>
        </>
      ) : (
        'N/A'
      )}
    </Text>
  )
}
