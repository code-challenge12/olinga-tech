import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Text } from '@chakra-ui/react'

import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'

import { Rating } from './Rating'

export default {
  title: 'Modules/BookShelf/Components/Fields/Rating',
  component: Rating,
  argTypes: {},
} as ComponentMeta<typeof Rating>

const Template: ComponentStory<typeof Rating> = (args) => {
  return (
    <Text>
      <Rating {...args} />
    </Text>
  )
}

export const Default = Template.bind({})

Default.args = {
  averageRating: __MOCK_BOOK_ITEM.volumeInfo.averageRating,
  ratingsCount: __MOCK_BOOK_ITEM.volumeInfo.ratingsCount,
}
