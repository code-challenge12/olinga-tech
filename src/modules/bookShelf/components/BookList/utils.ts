import { BookItem } from '@/modules/bookShelf/types'

export function getItemCount(items: BookItem[], _hasNextPage: boolean) {
  return items.length + 2
}

export function checkIsLastIndex(index: number, itemCount: number) {
  return index === itemCount - 1
}
export function checkIsFirstIndex(index: number) {
  return index === 0
}

export function getItemKey(index: number, items: BookItem[]) {
  const item = items[index]

  return item?.id ?? `index-${index}`
}
