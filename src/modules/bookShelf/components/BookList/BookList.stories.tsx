import { ComponentStory, ComponentMeta } from '@storybook/react'
import { useState } from 'react'
import { nanoid } from 'nanoid'

import { BookItem as BookItemType } from '@/modules/bookShelf/types'
import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'

import { BookShelfLayout } from '../../layouts/BookShelfLayout'

import { BookList } from './BookList'

export default {
  title: 'Modules/BookShelf/Components/BookList/BookList',
  component: BookList,
  argTypes: {},
  decorators: [
    (Story) => (
      <BookShelfLayout>
        <Story />
      </BookShelfLayout>
    ),
  ],
} as ComponentMeta<typeof BookList>

const Template: ComponentStory<typeof BookList> = (args) => {
  const [isLoadingNextPage, setLoadingNextPage] = useState(false)
  const [items, setItems] = useState<BookItemType[]>([])

  const loadNextPage = (...args: any[]) => {
    setLoadingNextPage(true)
    setTimeout(() => {
      setItems((items) => {
        return [...items].concat(
          new Array(10)
            .fill(true)
            .map(() => ({ ...__MOCK_BOOK_ITEM, id: nanoid() } as BookItemType)),
        )
      })
      setLoadingNextPage(false)
    }, 2500)
  }

  return (
    <BookList
      hasNextPage={items.length < 100}
      isLoadingNextPage={isLoadingNextPage}
      items={items}
      loadNextPage={loadNextPage}
    />
  )
}

export const Default = Template.bind({})

Default.args = {}
