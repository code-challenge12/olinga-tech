import {
  LinkBox,
  Stack,
  Box,
  LinkOverlay,
  Image,
  HStack,
  Text,
  Spacer,
  Divider,
} from '@chakra-ui/react'

import { CopyButton } from '@/components/CopyButton'

import { getImageSrc } from '@/modules/bookShelf/utils'
import { BookItem as BookItemType } from '@/modules/bookShelf/types'

import { ButtonGroup } from '@/modules/bookShelf/components/ButtonGroup'

import { PADDING_SIZES, IMAGE_WIDTH, CONTENT_HEIGHT, IMAGE_SX } from './styles'
import { Link } from '@/lib/link'

import { details } from '@/routes'

type Props = {
  bookData: BookItemType
  onClickLink?: React.MouseEventHandler<HTMLAnchorElement>
  onClickFavorite?: () => void
  onClickMarkAsRead?: () => void
  isFavorite?: boolean
  haveRead?: boolean
}

export function BookItem({
  bookData,
  isFavorite,
  haveRead,
  onClickLink,
  onClickFavorite,
  onClickMarkAsRead,
}: Props) {
  const { volumeInfo, saleInfo } = bookData
  const { title, previewLink, authors } = volumeInfo
  const { buyLink } = saleInfo

  return (
    <LinkBox
      sx={{
        width: 'full',
        position: 'relative',
        borderRadius: 'md',
        '&:hover > div': {
          backgroundColor: 'gray.50',
          boxShadow: 'xl',
        },
      }}
    >
      <Link
        href={{ pathname: details.pathname, query: { id: bookData.id } }}
        passHref
        legacyBehavior
        onClick={onClickLink}
      >
        <LinkOverlay href="#">
          <Image
            {...getImageSrc(bookData)}
            fallback={<Box sx={{ ...IMAGE_SX, bgColor: 'whiteAlpha.500' }} />}
            alt={title}
            placeholder="Cover Image"
            sx={IMAGE_SX}
          />
        </LinkOverlay>
      </Link>
      <Stack
        spacing="0"
        sx={{
          backgroundColor: 'white',
          borderRadius: 'md',
          padding: PADDING_SIZES,
          paddingLeft: IMAGE_WIDTH,
          marginLeft: PADDING_SIZES,
          marginTop: {
            base: 2,
            sm: 6,
          },
          boxShadow: 'lg',
          height: CONTENT_HEIGHT,
          transitionProperty: 'common',
          transitionDuration: 'normal',
          fontSize: { base: 'sm', sm: 'md' },
        }}
      >
        <Box>
          <HStack
            sx={{ alignItems: 'flex-start', justifyContent: 'space-between' }}
          >
            <Text
              noOfLines={2}
              sx={{ fontWeight: 'bold', fontSize: { base: 'md', sm: 'lg' } }}
            >
              {title}
            </Text>
            <CopyButton clipboardContent={title} />
          </HStack>

          <Text noOfLines={2} sx={{ mt: 2 }}>
            By{' '}
            <Text as="span" sx={{ fontStyle: 'italic' }}>
              {authors?.join(' & ') ?? '-'}
            </Text>
          </Text>

          <Divider sx={{ my: 2 }} />
          <Text
            noOfLines={{ base: 2, sm: 3 }}
            sx={{ fontSize: { base: 'xs', sm: 'sm' }, mt: 1 }}
          >
            {volumeInfo.description}
          </Text>
        </Box>
        <Spacer />
        <ButtonGroup
          previewLink={previewLink}
          buyLink={buyLink}
          haveRead={haveRead}
          isFavorite={isFavorite}
          onClickFavorite={onClickFavorite}
          onClickMarkAsRead={onClickMarkAsRead}
          sx={{ alignSelf: 'flex-end', mt: 2 }}
        />
      </Stack>
    </LinkBox>
  )
}
