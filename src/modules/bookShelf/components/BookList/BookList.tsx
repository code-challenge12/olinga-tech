import { useRef, useEffect } from 'react'
import { Box, Center, Spacer, useBreakpointValue } from '@chakra-ui/react'
import { VariableSizeList, ListChildComponentProps } from 'react-window'
import InfiniteLoader from 'react-window-infinite-loader'
import AutoSizer from 'react-virtualized-auto-sizer'

import { Loading } from '@/components/Loading'
import { NoData } from '@/components/NoData'
import { useNonInitialEffect } from '@/lib/hooks'
import { useFilterContext } from '@/contexts/FilterContext'

import { useFavoriteBooksContext } from '@/modules/bookShelf/contexts/FavoriteBooksContext'
import { useReadBooksContext } from '@/modules/bookShelf/contexts/ReadBooksContext'
import { setBookDetailData } from '@/modules/bookShelf/services/getBookDetails'
import { BookItem as BookItemType } from '@/modules/bookShelf/types'

import { BookItem } from './BookItem'
import { CONTENT_HEIGHT, PADDING_SIZES } from './styles'
import { getItemCount, checkIsFirstIndex } from './utils'

type Props = {
  isReliableData?: boolean
  hasNextPage?: boolean
  isLoadingNextPage?: boolean
  loadNextPage: () => void
  test?: BookItemType[]
  items: BookItemType[]
}

export function BookList({
  items,
  isReliableData = true,
  hasNextPage = false,
  isLoadingNextPage,
  loadNextPage,
}: Props) {
  const itemHeight = useBreakpointValue(CONTENT_HEIGHT, { ssr: false })
  const gutterSize = useBreakpointValue(PADDING_SIZES, { ssr: false })

  const itemCount = getItemCount(items, hasNextPage)

  const getItemKey = (index: number) => {
    const item = items[index]

    return item?.id ?? `index-${index}`
  }

  const getItemSize = (index: number) => {
    const _gutterSize = gutterSize ?? 4

    const isFirstIndex = checkIsFirstIndex(index)

    if (isFirstIndex) {
      return (_gutterSize + 8) * 8
    }

    return parseInt(itemHeight as string) + _gutterSize * 8
  }

  const checkIsItemLoaded = (index: number) =>
    !hasNextPage || index < items.length

  const loadMoreItems = isLoadingNextPage ? () => null : loadNextPage

  const infiniteLoaderRef = useRef<InfiniteLoader>(null)
  const varialbeSizeListRef = useRef<any>(null)

  const { searchValue } = useFilterContext()

  useEffect(() => {
    if (!varialbeSizeListRef.current) return

    varialbeSizeListRef.current.resetAfterIndex(0)
  }, [itemHeight, gutterSize])

  useNonInitialEffect(() => {
    if (infiniteLoaderRef.current) {
      infiniteLoaderRef.current.resetloadMoreItemsCache()
    }
  }, [searchValue])

  return (
    <Box sx={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
      {items.length === 0 && isLoadingNextPage ? (
        <Center layerStyle="pageWrapper">
          <Loading />
        </Center>
      ) : items.length === 0 ? (
        <Center layerStyle="pageWrapper">
          <NoData />
        </Center>
      ) : (
        <AutoSizer>
          {({ height, width }) => {
            return (
              <InfiniteLoader
                isItemLoaded={checkIsItemLoaded}
                itemCount={itemCount}
                loadMoreItems={loadMoreItems}
                ref={infiniteLoaderRef}
              >
                {({ onItemsRendered, ref }) => {
                  return (
                    <VariableSizeList
                      itemCount={itemCount}
                      onItemsRendered={onItemsRendered}
                      ref={(_ref) => {
                        ref(_ref)
                        varialbeSizeListRef.current = _ref
                      }}
                      width={width}
                      height={height}
                      itemKey={getItemKey}
                      itemSize={getItemSize}
                      estimatedItemSize={
                        parseInt(itemHeight as string) + (gutterSize ?? 4) * 8
                      }
                      itemData={{ items, hasNextPage, isReliableData }}
                    >
                      {Row}
                    </VariableSizeList>
                  )
                }}
              </InfiniteLoader>
            )
          }}
        </AutoSizer>
      )}
    </Box>
  )
}

type RowData = {
  isReliableData: boolean
  hasNextPage: boolean
  items: BookItemType[]
}

function Row({ index, style, data }: ListChildComponentProps<RowData>) {
  const { items, hasNextPage, isReliableData } = data
  const item = items[index - 1]

  const { readBooks, markAsRead, unmarkAsRead } = useReadBooksContext()
  const { favoriteBooks, addFavoriteBook, removeFavoriteBook } =
    useFavoriteBooksContext()

  const hasData = Boolean(item)
  const isFavorite = item ? Boolean(favoriteBooks[item.id]) : false
  const haveRead = item ? readBooks[item.id] : false

  const isFirstIndex = checkIsFirstIndex(index)

  if (isFirstIndex || (!hasData && !hasNextPage)) {
    return <Spacer style={style} />
  }

  if (!hasData) {
    return (
      <Center
        layerStyle="container"
        style={{ ...style, left: '50%', transform: 'translateX(-50%)' }}
      >
        <Loading />
      </Center>
    )
  }

  return (
    <Center
      layerStyle="container"
      style={{ ...style, left: '50%', transform: 'translateX(-50%)' }}
    >
      <BookItem
        bookData={item}
        isFavorite={isFavorite}
        haveRead={haveRead}
        onClickLink={
          isReliableData
            ? () => {
                setBookDetailData({ id: item.id }, item)
              }
            : undefined
        }
        onClickFavorite={
          !isFavorite
            ? () => addFavoriteBook(item)
            : () => removeFavoriteBook(item.id)
        }
        onClickMarkAsRead={
          !haveRead ? () => markAsRead(item.id) : () => unmarkAsRead(item.id)
        }
      />
    </Center>
  )
}
