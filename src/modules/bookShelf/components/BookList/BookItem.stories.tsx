import { ComponentStory, ComponentMeta } from '@storybook/react'
import { useState } from 'react'

import { useInterval } from '@/lib/hooks'

import { BookItem as BookItemType } from '@/modules/bookShelf/types'
import { __MOCK_BOOK_ITEM } from '@/modules/bookShelf/__mock__'

import { BookItem } from './BookItem'

export default {
  title: 'Modules/BookShelf/Components/BookList/BookItem',
  component: BookItem,
  argTypes: {},
} as ComponentMeta<typeof BookItem>

const Template: ComponentStory<typeof BookItem> = (args) => {
  const [isFavorite, setFavorite] = useState(false)

  useInterval(() => {
    setFavorite((isFavorite) => !isFavorite)
  }, 2000)

  return <BookItem {...args} isFavorite={isFavorite} />
}

export const Default = Template.bind({})

Default.args = {
  bookData: __MOCK_BOOK_ITEM as BookItemType,
}
