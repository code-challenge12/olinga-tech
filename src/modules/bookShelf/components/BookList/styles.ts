export const IMAGE_WIDTH = {
  base: '100px',
  sm: '200px',
}

export const CONTENT_HEIGHT = {
  base: '240px',
  sm: '320px',
}

export const PADDING_SIZES = {
  base: 4,
  sm: 8,
}

export const IMAGE_SX = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: IMAGE_WIDTH,
  height: CONTENT_HEIGHT,
  objectFit: 'contain',
  filter: 'drop-shadow(5px 5px 10px rgba(0, 0, 0, 0.2))',
  objectPosition: 'top left',
}
