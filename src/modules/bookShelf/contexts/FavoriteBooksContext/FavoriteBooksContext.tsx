import { useState, useEffect, useMemo, useCallback } from 'react'

import { createCtx } from '@/lib/utils'
import { localStorage } from '@/lib/storage'
import { useNonInitialEffect } from '@/lib/hooks'

import { BookItem } from '@/modules/bookShelf/types'

const FAVORITE_BOOKS_STORAGE_KEY = '__FAVORITE_BOOKS__'

type FavoriteBooks = Record<BookItem['id'], BookItem>

export type FavoriteBooksContext = {
  favoriteBooks: FavoriteBooks
  setFavoriteBooks: React.Dispatch<
    React.SetStateAction<FavoriteBooksContext['favoriteBooks']>
  >
  addFavoriteBook: (book: BookItem) => void
  removeFavoriteBook: (id: BookItem['id']) => void
}

export type FavoriteBooksContextProviderProps = {
  children: React.ReactNode
}

const favoriteBooksContext = createCtx<FavoriteBooksContext>()
const Provider = favoriteBooksContext[1]
export const useFavoriteBooksContext: () => FavoriteBooksContext =
  favoriteBooksContext[0]

export const FavoriteBooksContextProvider = ({
  children,
}: FavoriteBooksContextProviderProps): React.ReactElement => {
  const [isLoading, setLoading] = useState(true)
  const [favoriteBooks, setFavoriteBooks] = useState<FavoriteBooks>({})

  useEffect(() => {
    const storedFavoriteBooks = localStorage.getItem(FAVORITE_BOOKS_STORAGE_KEY)

    setFavoriteBooks((curr) => ({
      ...curr,
      ...(typeof storedFavoriteBooks === 'object'
        ? localStorage.getItem(FAVORITE_BOOKS_STORAGE_KEY)
        : {}),
    }))

    setLoading(false)
  }, [])

  const addFavoriteBook = useCallback<FavoriteBooksContext['addFavoriteBook']>(
    (book) => {
      setFavoriteBooks((curr) => {
        return {
          ...curr,
          [book.id]: book,
        }
      })
    },
    [],
  )

  const removeFavoriteBook = useCallback<
    FavoriteBooksContext['removeFavoriteBook']
  >((id) => {
    setFavoriteBooks((curr) => {
      const newList = {
        ...curr,
      }

      delete newList[id]

      return newList
    })
  }, [])

  const contextValue = useMemo(() => {
    return {
      favoriteBooks,
      setFavoriteBooks,
      addFavoriteBook,
      removeFavoriteBook,
    }
  }, [favoriteBooks, addFavoriteBook, removeFavoriteBook])

  useNonInitialEffect(() => {
    localStorage.setItem(FAVORITE_BOOKS_STORAGE_KEY, favoriteBooks)
  }, [favoriteBooks])

  return <Provider value={contextValue}>{!isLoading && children}</Provider>
}
