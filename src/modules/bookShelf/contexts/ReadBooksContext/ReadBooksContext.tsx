import { useState, useEffect, useMemo, useCallback } from 'react'

import { createCtx } from '@/lib/utils'
import { localStorage } from '@/lib/storage'
import { useNonInitialEffect } from '@/lib/hooks'

import { BookItem } from '@/modules/bookShelf/types'

const FAVORITE_BOOKS_STORAGE_KEY = '__READ_BOOKS__'

type ReadBooks = Record<BookItem['id'], boolean>

export type ReadBooksContext = {
  readBooks: ReadBooks
  setReadBooks: React.Dispatch<
    React.SetStateAction<ReadBooksContext['readBooks']>
  >
  markAsRead: (id: BookItem['id']) => void
  unmarkAsRead: (id: BookItem['id']) => void
}

export type ReadBooksContextProviderProps = {
  children: React.ReactNode
}

const readBooksContext = createCtx<ReadBooksContext>()
const Provider = readBooksContext[1]
export const useReadBooksContext: () => ReadBooksContext = readBooksContext[0]

export const ReadBooksContextProvider = ({
  children,
}: ReadBooksContextProviderProps): React.ReactElement => {
  const [isLoading, setLoading] = useState(true)
  const [readBooks, setReadBooks] = useState<ReadBooks>({})

  useEffect(() => {
    const storedReadBooks = localStorage.getItem(FAVORITE_BOOKS_STORAGE_KEY)

    setReadBooks((curr) => ({
      ...curr,
      ...(typeof storedReadBooks === 'object'
        ? localStorage.getItem(FAVORITE_BOOKS_STORAGE_KEY)
        : {}),
    }))

    setLoading(false)
  }, [])

  const markAsRead = useCallback<ReadBooksContext['markAsRead']>((id) => {
    setReadBooks((curr) => {
      return {
        ...curr,
        [id]: true,
      }
    })
  }, [])

  const unmarkAsRead = useCallback<ReadBooksContext['unmarkAsRead']>((id) => {
    setReadBooks((curr) => {
      const newList = {
        ...curr,
      }

      delete newList[id]

      return newList
    })
  }, [])

  const contextValue = useMemo(() => {
    return {
      readBooks,
      setReadBooks,
      markAsRead,
      unmarkAsRead,
    }
  }, [readBooks, markAsRead, unmarkAsRead])

  useNonInitialEffect(() => {
    localStorage.setItem(FAVORITE_BOOKS_STORAGE_KEY, readBooks)
  }, [readBooks])

  return <Provider value={contextValue}>{!isLoading && children}</Provider>
}
