import { useState, useMemo } from 'react'

import { createCtx } from '@/lib/utils'

export type LayoutContext = {
  isTransitioning: boolean
  setIsTransitioning: React.Dispatch<
    React.SetStateAction<LayoutContext['isTransitioning']>
  >
  isLoading: boolean
  setIsLoading: React.Dispatch<React.SetStateAction<LayoutContext['isLoading']>>
}

export type LayoutContextProviderProps = {
  children: React.ReactNode
}

const layoutContext = createCtx<LayoutContext>()
const Provider = layoutContext[1]
export const useLayoutContext: () => LayoutContext = layoutContext[0]

export const LayoutContextProvider = ({
  children,
}: LayoutContextProviderProps): React.ReactElement => {
  const [isTransitioning, setIsTransitioning] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const contextValue = useMemo(() => {
    return {
      isTransitioning,
      setIsTransitioning,
      isLoading,
      setIsLoading,
    }
  }, [isTransitioning, isLoading])

  return <Provider value={contextValue}>{children}</Provider>
}
