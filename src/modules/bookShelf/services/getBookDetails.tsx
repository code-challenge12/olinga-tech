import { useQuery, UseQueryOptions } from '@tanstack/react-query'

import requests from '@/lib/requests'
import { queryClient } from '@/lib/query'

import { BookItem } from '@/modules/bookShelf/types'

import { BASE_URL } from './constants'

const KEY = 'BOOK_DETAILS'
export const FIELDS = `
kind,
id,
etag,
volumeInfo(
  title,
  description,
  authors,
  publishedDate,
  publisher,
  industryIdentifiers,
  pageCount,
  printType,
  categories,
  averageRating,
  ratingsCount,
  imageLinks,
  language,
  previewLink
),
saleInfo
`
  .replace(/(?:\r\n|\r|\n|)/g, '')
  .replace(/\s/g, '')

type Params = {
  id: string
}

type ApiResponse = BookItem
type GetMethodResponse = BookItem

export async function getBookDetail({ id }: Params) {
  const { data } = await requests.get<ApiResponse>(`${BASE_URL}/volumes/:id`, {
    params: {
      id,
      fields: FIELDS,
    },
  })

  return data
}

export function useBookDetails(
  params: Partial<Params>,
  options?: UseQueryOptions<GetMethodResponse>,
) {
  return useQuery({
    queryKey: [KEY, params],
    queryFn: () => getBookDetail(params as Params),
    enabled: Boolean(params.id),
    ...options,
  })
}

export function setBookDetailData(params: Params, data: GetMethodResponse) {
  queryClient.setQueryData([KEY, params], data)
}
