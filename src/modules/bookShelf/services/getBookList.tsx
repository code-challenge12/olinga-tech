import {
  useInfiniteQuery,
  UseInfiniteQueryOptions,
} from '@tanstack/react-query'

import requests from '@/lib/requests'

import { useFilterContext } from '@/contexts/FilterContext'
import { BookItem } from '@/modules/bookShelf/types'

import { FIELDS as DETAILS_FIELD } from './getBookDetails'
import { BASE_URL } from './constants'

const DEFAULT_PER_PAGE = 20
const KEY = 'BOOK_LIST'
const FIELDS = `
kind,
totalItems,
items(
  ${DETAILS_FIELD}
)`
  .replace(/(?:\r\n|\r|\n|)/g, '')
  .replace(/\s/g, '')

type Params = {
  search?: string
  page?: number
  perPage?: number
}

type ApiResponse = {
  items: BookItem[]
  totalItems: number
  kind: string
}

type GetMethodResponse = BookItem[]

export async function getBookList({
  search = '',
  page = 1,
  perPage = DEFAULT_PER_PAGE,
}: Params = {}) {
  const { data } = await requests.get<ApiResponse>(`${BASE_URL}/volumes`, {
    params: {
      q: search,
      fields: FIELDS,
      startIndex: (page - 1) * perPage,
      maxResults: DEFAULT_PER_PAGE,
    },
    // headers: {
    //   'Accept-Encoding': 'gzip',
    //   'User-Agent': 'olinga-tech-challenge (gzip)',
    // },
  })

  return data.items ?? []
}

export function useInfiniteBookList(
  params?: Params,
  options?: UseInfiniteQueryOptions<GetMethodResponse>,
) {
  const { searchValue } = useFilterContext()

  const _params = {
    ...params,
    search: searchValue,
  }

  return useInfiniteQuery({
    queryKey: [KEY, _params],
    queryFn: ({ pageParam = 1 }) =>
      getBookList({ page: pageParam, ..._params }),
    ...options,
    getNextPageParam: (lastPage, allPage) => {
      return lastPage?.length === DEFAULT_PER_PAGE
        ? allPage.length + 1
        : undefined
    },
    getPreviousPageParam: (firstPage) => {
      return firstPage?.length === DEFAULT_PER_PAGE ? true : undefined
    },
    enabled: Boolean(searchValue),
  })
}
