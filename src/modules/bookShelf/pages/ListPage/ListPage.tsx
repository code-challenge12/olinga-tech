import { Center } from '@chakra-ui/react'
import { useTransition } from '@react-spring/web'

import { BookShelfLayout } from '@/modules/bookShelf/layouts/BookShelfLayout'
import { useInfiniteBookList } from '@/modules/bookShelf/services/getBookList'
import { useFilterContext } from '@/contexts/FilterContext'
import { BookList } from '@/modules/bookShelf/components/BookList'

import { DEFAULT_SPRING_CONFIG, AnimatedBox } from '@/lib/animation'

export type Props = {}

export function ListPage() {
  const { data, isFetching, fetchNextPage, hasNextPage } = useInfiniteBookList()

  const { searchValue } = useFilterContext()

  const transitions = useTransition(searchValue, {
    from: {
      opacity: 0,
    },
    enter: {
      opacity: 1,
    },
    leave: {
      opacity: 0,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
    },
    config: DEFAULT_SPRING_CONFIG,
  })

  return transitions((style, searchValue) => {
    return searchValue ? (
      <AnimatedBox style={style}>
        <BookList
          hasNextPage={hasNextPage}
          isLoadingNextPage={isFetching}
          loadNextPage={fetchNextPage}
          items={data?.pages.flat() ?? []}
        />
      </AnimatedBox>
    ) : (
      <AnimatedBox as={Center} layerStyle="pageWrapper" style={style}>
        Use search field up top to find your book.
      </AnimatedBox>
    )
  })
}

ListPage.getLayout = function getLayout(page: React.ReactNode) {
  return <BookShelfLayout isSearchable>{page}</BookShelfLayout>
}
