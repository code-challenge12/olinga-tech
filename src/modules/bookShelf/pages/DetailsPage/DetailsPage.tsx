import { useRouter } from 'next/router'
import { VStack, Divider } from '@chakra-ui/react'

import { RenderData } from '@/components/Loading'
import { NoData } from '@/components/NoData'
import { BackButton } from '@/components/BackButton'

import { BookShelfLayout } from '@/modules/bookShelf/layouts/BookShelfLayout'
import { useFavoriteBooksContext } from '@/modules/bookShelf/contexts/FavoriteBooksContext'
import { useReadBooksContext } from '@/modules/bookShelf/contexts/ReadBooksContext'
import { useBookDetails } from '@/modules/bookShelf/services/getBookDetails'
import { BookItem } from '@/modules/bookShelf/types'
import {
  BookDetailsContent,
  BookDetailsHeader,
} from '@/modules/bookShelf/components/BookDetails'

export type Props = {
  bookData: BookItem
}

export function DetailsPage() {
  const { query } = useRouter()

  const { data: book, isLoading } = useBookDetails({ id: query.id as string })

  const { favoriteBooks, addFavoriteBook, removeFavoriteBook } =
    useFavoriteBooksContext()
  const { readBooks, markAsRead, unmarkAsRead } = useReadBooksContext()

  return (
    <RenderData
      isLoading={isLoading}
      render={() => {
        if (!book)
          return (
            <VStack layerStyle="pageWrapper">
              <NoData />
            </VStack>
          )

        const isFavorite = Boolean(favoriteBooks[book.id])
        const haveRead = Boolean(readBooks[book.id])

        const onClickFavorite = !isFavorite
          ? () => addFavoriteBook(book)
          : () => removeFavoriteBook(book.id)
        const onClickMarkAsRead = !haveRead
          ? () => markAsRead(book.id)
          : () => unmarkAsRead(book.id)

        return (
          <VStack layerStyle="pageWrapper">
            {
              <>
                <BookDetailsHeader
                  bookData={book}
                  isFavorite={isFavorite}
                  haveRead={haveRead}
                  onClickFavorite={onClickFavorite}
                  onClickMarkAsRead={onClickMarkAsRead}
                />
                <BookDetailsContent
                  bookData={book}
                  isFavorite={isFavorite}
                  haveRead={haveRead}
                  onClickFavorite={onClickFavorite}
                  onClickMarkAsRead={onClickMarkAsRead}
                />
                <Divider layerStyle="container" sx={{ padding: 0 }} />
                <BackButton />
              </>
            }
          </VStack>
        )
      }}
    />
  )
}

DetailsPage.getLayout = function getLayout(
  page: React.ReactNode,
  { bookData }: Props,
) {
  return (
    <BookShelfLayout
      pageMeta={{
        title: bookData.volumeInfo.title,
        description: bookData.volumeInfo.description,
      }}
    >
      {page}
    </BookShelfLayout>
  )
}
