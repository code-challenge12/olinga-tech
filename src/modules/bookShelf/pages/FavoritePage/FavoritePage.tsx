import { useMemo } from 'react'
import { useTransition } from '@react-spring/web'

import { useFilterContext } from '@/contexts/FilterContext'
import { BookShelfLayout } from '@/modules/bookShelf/layouts/BookShelfLayout'
import { useFavoriteBooksContext } from '@/modules/bookShelf/contexts/FavoriteBooksContext'
import { BookList } from '@/modules/bookShelf/components/BookList'
import { filterVolume } from '@/modules/bookShelf/utils'

import { DEFAULT_SPRING_CONFIG, AnimatedBox } from '@/lib/animation'

export type Props = {}

export function FavoritePage() {
  const { favoriteBooks } = useFavoriteBooksContext()

  const { searchValue } = useFilterContext()

  const books = useMemo(() => {
    const books = Object.values(favoriteBooks)

    return searchValue
      ? books.filter((book) => filterVolume(searchValue, book))
      : books
  }, [favoriteBooks, searchValue])

  const transitions = useTransition(searchValue, {
    from: {
      opacity: 0,
    },
    enter: {
      opacity: 1,
    },
    leave: {
      opacity: 0,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
    },
    config: DEFAULT_SPRING_CONFIG,
  })

  return transitions((style) => {
    return (
      <AnimatedBox style={style}>
        <BookList
          hasNextPage={false}
          isLoadingNextPage={false}
          loadNextPage={() => null}
          items={books}
          test={books}
        />
      </AnimatedBox>
    )
  })
}

FavoritePage.getLayout = function getLayout(page: React.ReactNode) {
  return (
    <BookShelfLayout isSearchable pageMeta={{ title: 'My Favorite' }}>
      {page}
    </BookShelfLayout>
  )
}
