import { NextSeo } from 'next-seo'

import { LayoutContextProvider } from '@/modules/bookShelf/contexts/LayoutContext'
import { FavoriteBooksContextProvider } from '@/modules/bookShelf/contexts/FavoriteBooksContext'
import { ReadBooksContextProvider } from '@/modules/bookShelf/contexts/ReadBooksContext'
import {
  FilterContextProvider,
  useFilterContext,
} from '@/contexts/FilterContext'

import { NavBar } from '@/components/NavBar'

import { PageMeta } from '@/types/pages'

type Props = {
  children: React.ReactNode
  pageMeta?: PageMeta
  isSearchable?: boolean
}

export function BookShelfLayout({
  children,
  pageMeta = {},
  isSearchable = false,
}: Props) {
  return (
    <>
      <NextSeo {...pageMeta} />
      <LayoutContextProvider>
        <FilterContextProvider>
          <FavoriteBooksContextProvider>
            <ReadBooksContextProvider>
              <BookShelfLayoutComponent isSearchable={isSearchable}>
                {children}
              </BookShelfLayoutComponent>
            </ReadBooksContextProvider>
          </FavoriteBooksContextProvider>
        </FilterContextProvider>
      </LayoutContextProvider>
    </>
  )
}

function BookShelfLayoutComponent({
  children,
  isSearchable,
}: Omit<Props, 'pageMeta'>) {
  const { setSearchValue, searchValue } = useFilterContext()

  return (
    <>
      <NavBar
        onSearch={isSearchable ? setSearchValue : undefined}
        defaultSearchValue={searchValue}
      />
      {children}
    </>
  )
}
