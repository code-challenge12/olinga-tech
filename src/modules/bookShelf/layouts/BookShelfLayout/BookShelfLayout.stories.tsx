import { ComponentStory, ComponentMeta } from '@storybook/react'

import { BookShelfLayout } from './BookShelfLayout'

export default {
  title: 'Modules/BookShelf/Layouts/BookShelfLayout',
  component: BookShelfLayout,
  argTypes: {
    isSearchable: { control: 'boolean' },
  },
} as ComponentMeta<typeof BookShelfLayout>

const Template: ComponentStory<typeof BookShelfLayout> = (args) => {
  return <BookShelfLayout {...args} />
}

export const Default = Template.bind({})

Default.args = {
  isSearchable: true,
}
