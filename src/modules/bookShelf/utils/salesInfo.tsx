import { PriceInfo } from '@/modules/bookShelf/types'

export function comparePrice(retailPrice?: PriceInfo, listPrice?: PriceInfo) {
  if (!retailPrice || !listPrice) return true

  if (retailPrice.currencyCode !== listPrice.currencyCode) return false
  if (retailPrice.amount !== listPrice.amount) return false

  return true
}
