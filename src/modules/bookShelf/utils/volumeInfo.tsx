import { isContainText } from '@/lib/utils'

import { BookItem } from '@/modules/bookShelf/types'

export function getImageSrc(
  item: BookItem,
  options: { width: number; height: number; fallbackSrc?: string } = {
    width: 400,
    height: 600,
    fallbackSrc: '',
  },
) {
  return {
    src: `https://books.google.com/books/publisher/content/images/frontcover/${item.id}?fife=w${options.width}-h${options.height}&source=gbs_api`,
    fallbackSrc: options.fallbackSrc,
  }
}

export function filterVolume(searchText: string, item: BookItem) {
  const { volumeInfo } = item

  if (isContainText(volumeInfo.title, searchText)) return true

  if (volumeInfo.authors?.some((author) => isContainText(author, searchText)))
    return true

  if (
    volumeInfo.industryIdentifiers?.some((indentifier) =>
      isContainText(indentifier.identifier, searchText),
    )
  )
    return true

  return false
}
