export type IndustryIdentifier = {
  type: 'ISBN_10' | 'ISBN_13' | 'ISSN' | 'OTHER'
  identifier: string
}

export type VolumeInfo = {
  title: string
  authors: string[]
  publisher: string
  publishedDate: string
  description: string
  industryIdentifiers: IndustryIdentifier[]
  // readingModes: {
  //   text: boolean
  //   image: boolean
  // }
  pageCount: number
  printType: 'BOOK' | 'MAGAZINE'
  categories: string[]
  averageRating?: number
  ratingsCount?: number
  maturityRating: string
  // allowAnonLogging: boolean
  // contentVersion: string
  // panelizationSummary: {
  //   containsEpubBubbles: boolean
  //   containsImageBubbles: boolean
  // }
  imageLinks: {
    smallThumbnail: string
    thumbnail: string
  }
  language: string
  previewLink: string
  // infoLink: string
  // canonicalVolumeLink: string
}

export type PriceInfo = {
  amount: number
  currencyCode: string
}

export type SaleInfo = {
  country: string
  isEbook: boolean
  saleability: 'FOR_SALE' | 'FOR_PREORDER' | 'NOT_FOR_SALE' | 'FREE'
  listPrice?: PriceInfo
  retailPrice?: PriceInfo
  buyLink?: string
}

export type AccessInfo = unknown

export type SearchInfo = {
  textSnippet: string
}

export type BookItem = {
  kind: string
  id: string
  etag: string
  selfLink: string
  volumeInfo: VolumeInfo
  saleInfo: SaleInfo
  // accessInfo: AccessInfo
  // searchInfo: SearchInfo
}
