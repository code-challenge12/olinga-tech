import { ErrorBoundary as ReactErrorBoundary } from 'react-error-boundary'
import { useQueryErrorResetBoundary } from '@tanstack/react-query'
import { ReactNode } from 'react'

import { FullPageError } from '@/modules/errors/pages/FullPageError'

type Props = {
  children: ReactNode
}

export const ErrorBoundary = (props: Props) => {
  const { children } = props
  const { reset } = useQueryErrorResetBoundary()

  return (
    <ReactErrorBoundary
      onReset={reset}
      fallbackRender={(errorProps) => {
        return <FullPageError {...errorProps} />
      }}
    >
      {children}
    </ReactErrorBoundary>
  )
}
