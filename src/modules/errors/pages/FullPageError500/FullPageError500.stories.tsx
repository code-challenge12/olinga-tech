import { ComponentStory, ComponentMeta } from '@storybook/react'

import { FullPageError500 } from './FullPageError500'

export default {
  title: 'Modules/Errors/Pages/FullPageError500',
  component: FullPageError500,
  argTypes: {},
} as ComponentMeta<typeof FullPageError500>

const Template: ComponentStory<typeof FullPageError500> = (args) => {
  return <FullPageError500 />
}

export const Default = Template.bind({})

Default.args = {}
