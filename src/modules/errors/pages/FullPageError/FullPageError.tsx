import { FallbackProps } from 'react-error-boundary'

import { FullPageError500 } from '@/modules/errors/pages/FullPageError500'
import { FullPageError400 } from '@/modules/errors/pages/FullPageError400'

export const FullPageError = (errorProps: FallbackProps) => {
  const { error, resetErrorBoundary } = errorProps
  const statusCode = (error as any)?.response?.status ?? 500

  if (statusCode >= 500) {
    return <FullPageError500 onClickLink={resetErrorBoundary} />
  }

  if (statusCode >= 400) {
    return <FullPageError400 onClickLink={resetErrorBoundary} />
  }

  return <FullPageError500 onClickLink={resetErrorBoundary} />
}
