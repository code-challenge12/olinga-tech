import { Center, Box, Text, Button, VStack } from '@chakra-ui/react'
import { Global } from '@emotion/react'
import NextImage from 'next/image'

import { Link } from '@/lib/link'

import Ghost from '@/modules/errors/assets/ghost-2.svg'

type Props = {
  onClickLink?: () => void
}

export function FullPageError400({ onClickLink }: Props) {
  return (
    <Center sx={{ minH: '100vh' }}>
      <Global
        styles={{
          body: {
            backgroundColor: 'var(--chakra-colors-gray-50)',
          },
        }}
      />
      <VStack spacing="10">
        <Box
          sx={{
            boxSize: '300px',
          }}
        >
          <NextImage src={Ghost} alt="ghost" width={300} height={300} />
        </Box>
        <VStack spacing="6">
          <Box sx={{ textAlign: 'center' }}>
            <Text
              as="h2"
              sx={{
                fontSize: { base: '2xl', sm: '3xl' },
                fontWeight: 'bold',
              }}
            >
              Boo!
            </Text>
            <Text
              sx={{
                fontSize: { base: 'xl', sm: '2xl' },
              }}
            >
              There nothing to be found here... except me!
            </Text>
            <Text fontSize="16px" color="#888" fontWeight="300">
              Use the button below to guild your home.
            </Text>
          </Box>
          <Link
            passHref
            legacyBehavior
            sx={{
              textDecoration: 'none !important',
              mt: 4,
              color: 'gray.700',
            }}
            onClick={onClickLink}
            to="home"
          >
            <Button as="a" variant="outline" colorScheme="blackAlpha" size="lg">
              Back Homepage
            </Button>
          </Link>
        </VStack>
      </VStack>
    </Center>
  )
}
