import { ComponentStory, ComponentMeta } from '@storybook/react'

import { FullPageError400 } from './FullPageError400'

export default {
  title: 'Modules/Errors/Layouts/FullPageError400',
  component: FullPageError400,
  argTypes: {},
} as ComponentMeta<typeof FullPageError400>

const Template: ComponentStory<typeof FullPageError400> = (args) => {
  return <FullPageError400 />
}

export const Default = Template.bind({})

Default.args = {}
