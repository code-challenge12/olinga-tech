import { useState, useMemo, useDeferredValue } from 'react'
import { useRouter } from 'next/router'
import queryString from 'query-string'

import { createCtx } from '@/lib/utils'
import { useNonInitialEffect } from '@/lib/hooks'

export type FilterContext = {
  searchValue: string
  setSearchValue: React.Dispatch<
    React.SetStateAction<FilterContext['searchValue']>
  >
}

export type FilterContextProviderProps = {
  children: React.ReactNode
}

const filterContext = createCtx<FilterContext>()
const Provider = filterContext[1]
export const useFilterContext: () => FilterContext = filterContext[0]

export const FilterContextProvider = ({
  children,
}: FilterContextProviderProps): React.ReactElement => {
  const router = useRouter()

  const getDefaultSearchValue = () => {
    const parsedUrl = queryString.parseUrl(router.asPath)

    return parsedUrl?.query.search?.toString() ?? ''
  }

  const [searchValue, setSearchValue] = useState(getDefaultSearchValue)

  const deferredSearchValue = useDeferredValue(searchValue)

  const contextValue = useMemo(() => {
    return {
      searchValue: deferredSearchValue,
      setSearchValue,
    }
  }, [deferredSearchValue])

  useNonInitialEffect(() => {
    setSearchValue(getDefaultSearchValue)
  }, [router.pathname])

  useNonInitialEffect(() => {
    const query = {
      ...router.query,
      search: deferredSearchValue,
    }

    router.replace(
      {
        pathname: router.pathname,
        query: query,
      },
      undefined,
      {
        shallow: true,
      },
    )
  }, [deferredSearchValue])

  return <Provider value={contextValue}>{children}</Provider>
}
