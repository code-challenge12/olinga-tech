import { GetStaticPropsContext, GetStaticPropsResult } from 'next'

import {
  FavoritePage,
  Props as FavoritePageProps,
} from '@/modules/bookShelf/pages/FavoritePage'

export async function getStaticProps(
  _: GetStaticPropsContext,
): Promise<GetStaticPropsResult<FavoritePageProps>> {
  return {
    props: {},
  }
}

export default FavoritePage
