import { GetStaticPropsContext, GetStaticPropsResult } from 'next'

import { FullPageError400 } from '@/modules/errors/pages/FullPageError400'

export async function getStaticProps(
  _: GetStaticPropsContext,
): Promise<GetStaticPropsResult<{}>> {
  return {
    props: {},
  }
}

export default FullPageError400
