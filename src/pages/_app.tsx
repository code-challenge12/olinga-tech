import { AppProps } from 'next/app'
import { DefaultSeo } from 'next-seo'
import { useRouter } from 'next/router'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'

import { ThemeProvider } from '@/lib/styles/ThemeProvider'
import { ErrorBoundary } from '@/modules/errors/components/ErrorBoundary'
import { PageTransition } from '@/components/PageTransition'

import { NextComponentWithLayoutType } from '@/types/pages'

import env from '@/lib/env'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: true,
      refetchOnWindowFocus: false,
      useErrorBoundary: true,
      staleTime: 5000,
    },
  },
})

function MyApp(props: AppProps): React.ReactElement {
  const { pageProps } = props
  const Component: NextComponentWithLayoutType = props.Component
  const getLayout = Component.getLayout || ((page) => page)

  const router = useRouter()

  return (
    <>
      <DefaultSeo
        title="Book Shelf"
        titleTemplate="%s | Olinga Tech Challenge"
        canonical={`${env.APP_URL}${router.asPath}`}
      />
      <ThemeProvider>
        <QueryClientProvider client={queryClient}>
          <ErrorBoundary>
            {getLayout(
              <PageTransition>
                <Component {...pageProps} />
              </PageTransition>,
              pageProps,
            )}
          </ErrorBoundary>
          <ReactQueryDevtools position="bottom-right" initialIsOpen={false} />
        </QueryClientProvider>
      </ThemeProvider>
    </>
  )
}

export default MyApp
