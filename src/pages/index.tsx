import { GetStaticPropsContext, GetStaticPropsResult } from 'next'

import {
  ListPage,
  Props as ListPageProps,
} from '@/modules/bookShelf/pages/ListPage'

export async function getStaticProps(
  _: GetStaticPropsContext,
): Promise<GetStaticPropsResult<ListPageProps>> {
  return {
    props: {},
  }
}

export default ListPage
