import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document<{}> {
  render(): JSX.Element {
    return (
      <Html lang="th">
        <Head>
          <link rel="shortcut icon" href="/images/favicon.svg" />

          <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&amp;display=swap"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <div id="app-portal">
            <div id="app-modal-portal"></div>
          </div>
          <NextScript />
          <noscript></noscript>
        </body>
      </Html>
    )
  }
}
