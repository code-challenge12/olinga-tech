import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next'

import {
  DetailsPage,
  Props as DetailsPageProps,
} from '@/modules/bookShelf/pages/DetailsPage'

import { getBookDetail } from '@/modules/bookShelf/services/getBookDetails'

export async function getServerSideProps(
  ctx: GetServerSidePropsContext,
): Promise<GetServerSidePropsResult<DetailsPageProps>> {
  const { query } = ctx

  try {
    const data = await getBookDetail({
      id: query.id as string,
    })

    if (!data) throw new Error('not found')

    return {
      props: {
        bookData: data,
      },
    }
  } catch (error) {}

  return {
    props: {} as DetailsPageProps,
    notFound: true,
  }
}

export default DetailsPage
