type ServerRuntimeConfig = {}

declare module 'next/config' {
  export default (): {
    serverRuntimeConfig: ServerRuntimeConfig
    publicRuntimeConfig: unknown
  } => ({
    serverRuntimeConfig,
    publicRuntimeConfig,
  })
}
