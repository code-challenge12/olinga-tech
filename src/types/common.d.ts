// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Dict<T = any> = Record<string, T>

type RequireAtLeastOne<T> = {
  [K in keyof T]-?: Required<Pick<T, K>> & Partial<Pick<T, Exclude<keyof T, K>>>
}[keyof T]
