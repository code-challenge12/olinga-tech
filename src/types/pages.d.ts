import { NextComponentType } from 'next'

import { NextSeoProps } from 'next-seo'

export type NextComponentWithLayoutType = {
  getLayout?: (page: React.ReactNode, pageProps: any) => React.ReactNode
} & NextComponentType

export type PageMeta = Partial<NextSeoProps>
