declare module '*.svg' {
  export const ReactComponent: React.FC<React.SVGAttributes<SVGElement>>

  const path: string
  export default path
}
