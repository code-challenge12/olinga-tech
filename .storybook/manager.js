// .storybook/manager.js

import { addons } from '@storybook/addons'

addons.setConfig({
  showNav: true,
  showPanel: true,
  panelPosition: 'right',
  sidebarAnimations: true,
  enableShortcuts: true,
  isToolshown: true,
})
