import { Suspense } from 'react'
import { Global, css } from '@emotion/react'
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'
import { QueryClientProvider } from '@tanstack/react-query'
import { RouterContext } from 'next/dist/shared/lib/router-context'
import * as NextImage from 'next/image'

import { ThemeProvider } from '../src/lib/styles/ThemeProvider'
import { queryClient } from '../src/lib/query'

const OriginalNextImage = NextImage.default

Object.defineProperty(NextImage, 'default', {
  configurable: true,
  value: (props) => {
    return (
      <OriginalNextImage
        {...props}
        unoptimized
        blurDataURL="data:image/jpeg;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAADAAQDASIAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAf/xAAbEAADAAMBAQAAAAAAAAAAAAABAgMABAURUf/EABUBAQEAAAAAAAAAAAAAAAAAAAMF/8QAFxEAAwEAAAAAAAAAAAAAAAAAAAECEf/aAAwDAQACEQMRAD8Anz9voy1dCI2mectSE5ioFCqia+KCwJ8HzGMZPqJb1oPEf//Z"
      />
    )
  },
})

const withQueryProvider = (Story, context) => {
  return (
    <QueryClientProvider client={queryClient}>
      <Story {...context} />
    </QueryClientProvider>
  )
}

const withThemeProvider = (Story, context) => {
  return (
    <>
      <Global
        styles={css`
          .sb-show-main {
            padding: 0 !important;
            margin: 0;
          }
          .sb-show-main > #root {
            width: 100%;
            height: 100%;
            padding: 0 !important;
            margin: 0;
            display: flex;
          }
        `}
      />
      <ThemeProvider>
        <Story {...context} />
      </ThemeProvider>
    </>
  )
}

const withContainer = (Story, context) => {
  return (
    <>
      <div
        style={{
          minWidth: '100%',
          minHeight: '100vh',
          margin: '0',
          padding: '0',
          overflow: 'auto',
        }}
      >
        <div
          style={{
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '0',
            padding: '0',
            overflow: 'visible',
            flexFlow: 'column nowrap',
          }}
        >
          <Story {...context} />
        </div>
      </div>
      <div id="app-portal">
        <div id="app-modal-portal"></div>
      </div>
    </>
  )
}

export const decorators = [withContainer, withThemeProvider, withQueryProvider]

export const parameters = {
  layout: 'centered',
  controls: { expanded: true },
  docs: {
    // theme: themes.normal,
    disable: true,
  },
  actions: { argTypesRegex: '^on[A-Z].*' },
  viewport: {
    viewports: INITIAL_VIEWPORTS, // newViewports would be an ViewportMap. (see below for examples)
    // defaultViewport: "responsive",
  },
  previewTabs: {
    'storybook/docs/panel': {
      hidden: true,
    },
    canvas: {
      title: 'Story',
      hidden: false,
    },
  },
  options: {
    storySort: {
      order: ['Introduction', 'Intro', 'UI Kits', 'Common', 'Components'],
      method: 'alphabetical',
    },
  },
  nextRouter: {
    Provider: RouterContext.Provider,
  },
}
