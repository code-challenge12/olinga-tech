const path = require('path')
const ESLintPlugin = require('eslint-webpack-plugin')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

const toPath = (_path) => path.join(process.cwd(), _path)

module.exports = {
  core: {
    builder: {
      name: 'webpack5',
      options: {
        lazyCompilation: true,
      },
    },
  },
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-essentials', 'storybook-addon-next-router'],
  framework: '@storybook/react',
  typescript: {
    check: false,
    checkOptions: {},
    reactDocgen: 'none',
  },
  webpackFinal: async (config) => {
    const fileLoaderRule = config.module.rules.find(
      (rule) =>
        rule &&
        rule.test &&
        !Array.isArray(rule.test) &&
        rule.test.test('test.svg'),
    )

    if (fileLoaderRule) {
      fileLoaderRule.exclude = /\.(svg|png|jpg|jpeg)$/i
    }

    const newConfig = {
      ...config,
      plugins: [
        ...(config.plugins ?? []),

        // new ESLintPlugin({
        //   extensions: ['js', 'jsx', 'ts', 'tsx'],
        //   emitWarning: true,
        //   failOnError: false,
        // }),
      ],
      module: {
        ...config.module,
        rules: [
          ...config.module.rules,
          {
            test: /\.svg$/i,
            use: [
              {
                loader: '@svgr/webpack',
                options: {
                  typescript: true,
                  tsx: true,
                  dimensions: false,
                  icon: true,
                  svgoConfig: {
                    plugins: [
                      {
                        name: 'preset-default',
                        params: {
                          overrides: {
                            cleanupIDs: false,
                            prefixIds: false,
                          },
                        },
                      },
                    ],
                  },
                },
              },
              'url-loader',
            ],
          },
          {
            test: /\.(png|jpg|jpeg)/i,
            use: ['url-loader'],
          },
        ],
      },
      resolve: {
        ...(config.resolve ?? {}),
        modules: [
          ...(config.resolve.modules ?? []),
          toPath('node_modules'),
          toPath('src'),
        ],
        alias: {
          ...(config.resolve.alias ?? {}),
          '@emotion/core': toPath('node_modules/@emotion/react'),
          '@emotion/styled': toPath('node_modules/@emotion/styled'),
          'emotion-theming': toPath('node_modules/@emotion/react'),
        },
        plugins: [
          ...(config.resolve.plugins ?? []),
          new TsconfigPathsPlugin({
            configFile: toPath('tsconfig.json'),
          }),
        ],
      },
    }

    return newConfig
  },
}
