module.exports = {
  i18n: {
    defaultLocale: 'ja',
    fallbackLng: 'en',
    locales: ['en', 'ja'],
    localeExtension: 'yaml',
    localeDetection: false,
    reloadOnPrerender: false,
  },
  react: {
    useSuspense: false,
  },
}
