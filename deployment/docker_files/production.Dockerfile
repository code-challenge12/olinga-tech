
# Install dependencies only when needed
FROM node:18.12-bullseye-slim AS deps
WORKDIR /srv/src/app

COPY package.json yarn.lock ./
RUN yarn install --immutable

# base image
FROM node:18.12-bullseye-slim AS builder
WORKDIR /srv/src/app
COPY . .
COPY --from=deps /srv/src/app/node_modules ./node_modules
COPY ./deployment/envs/production.env /srv/src/app/.env

RUN yarn build

# Production image, copy all the files and run next
FROM node:18.12-bullseye-slim AS runner
WORKDIR /srv/src/app

ENV NODE_ENV production
EXPOSE 8081

COPY --from=builder /srv/src/app/package.json ./
COPY --from=builder /srv/src/app/next.config.js ./
COPY --from=builder /srv/src/app/public ./public
COPY --from=builder /srv/src/app/.next ./.next
COPY --from=builder /srv/src/app/node_modules ./node_modules
COPY --from=builder /srv/src/app/deployment ./deployment

CMD ["yarn", "start"]
