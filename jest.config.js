const jestConfig = {
  roots: ['<rootDir>'],
  moduleFileExtensions: ['js', 'ts', 'tsx', 'json'],
  preset: 'ts-jest',
  moduleDirectories: [
    'node_modules',
    // add the directory with the test-utils.js file, for example:
    'tests', // a utility folder
    __dirname, // the root directory
  ],
  collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}'],
  testPathIgnorePatterns: ['<rootDir>[/\\\\](node_modules|.next)[/\\\\]'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(ts|tsx)$'],
  setupFilesAfterEnv: ['<rootDir>/tests/setup-env.js'],
  transform: {
    '^.+\\.(t|j)sx?$': [
      '@swc/jest',
      {
        jsc: {
          baseUrl: '.',
          paths: {
            '@/svg/*': ['src/assets/svg/*'],
            '@/png/*': ['src/assets/png/*'],
            '@/components/*': ['src/components/*'],
            '@/contexts/*': ['src/contexts/*'],
            '@/lib/*': ['src/lib/*'],
            '@/routes': ['src/routes'],
            '@/routes/*': ['src/routes/*'],
            '@/modules/*': ['src/modules/*'],
            '@/types/*': ['src/types/*'],
            'test-utils': ['tests/test-utils'],
          },
          transform: {
            react: {
              runtime: 'automatic',
            },
          },
        },
      },
    ],
  },
  testEnvironment: 'jsdom',
}

module.exports = jestConfig
